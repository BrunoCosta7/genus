package InterfacesController;

import java.util.List;

import model.Cidade;
import model.Endereco;
import model.Estado;
import utils.Result;

public interface IEnderecoController{
	
	public Result salvar(Endereco endereco);
	public Result excluir(Endereco endereco);
	public Result alterar(Endereco endereco);
	
	public Endereco pesquisar(String cpf_cnpj_id_usuario);
	public List<Endereco> filtrar(String nome);
	
	public void validarRegrasAntesIncluir();
	public void validarRegrasAntesAlterar();
	
	public Result verificarEstadoCidade(Cidade cidade);
	public List<Cidade> getCidades(String idEstado);
	public List<Estado> getEstados();
	
}
