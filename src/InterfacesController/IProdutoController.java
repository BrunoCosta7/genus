package InterfacesController;

import java.util.List;

import model.Produto;
import utils.Result;

public interface IProdutoController{
	
	public Result salvar(Produto produto);
	public Result excluir(Produto Produto);
	public Result alterar(Produto produto);
	public List<Produto> listObjects();
	public Produto pesquisar(Integer id_produto);
	public Produto pesquisarPorId(Integer id_produto);
	public List<Produto> filtrar(String nome, String medida);
	
	
	public Result validarRegrasAntesIncluir(Produto produto);
	public Result validarRegrasAntesAlterar(Produto produto);
	
}
