package InterfacesController;

import java.util.List;

import model.Historico;

public interface IHistoricoController {
	public List<Historico> listarTodosHistoricos();
	public List<Historico> filtrar(String nomeProduto, String dataInicial, String dataFinal);
}
