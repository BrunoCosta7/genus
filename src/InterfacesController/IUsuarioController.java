package InterfacesController;

import java.util.List;

import model.Usuario;
import utils.Result;

public interface IUsuarioController{
	
	public Result salvar(Usuario usuario);
	public Result excluir(Usuario usuario);
	public Result alterar(Usuario usuario);
	public List<Usuario> listObjects();
	public Usuario pesquisar(String cpf_cnpj_id_usuario);
	public List<Usuario> filtrar(String nome);
	public Usuario pesquisarUsuarioPorId(Usuario usuarioAux);
	public Result validarRegrasAntesIncluir(Usuario usuario);
	public Result validarRegrasAntesAlterar(Usuario usuario);
	
}
