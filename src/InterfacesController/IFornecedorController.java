package InterfacesController;

import java.util.List;

import model.Fornecedor;
import utils.Result;

public interface IFornecedorController{
	
	public Result salvar(Fornecedor fornecedor);
	public Result excluir(Fornecedor fornecedor);
	public Result alterar(Fornecedor fornecedor);
	public List<Fornecedor> listObjects();
	public Fornecedor pesquisar(String cpf_cnpj_id_fornecedor);
	public Fornecedor pesquisarPorNome(String nomeFornecedor);
	public List<Fornecedor> filtrar(String nome);
	public Fornecedor exist(Fornecedor fornecedor);
	public Result validarRegrasAntesIncluir(Fornecedor fornecedor);
	public Result validarRegrasAntesAlterar(Fornecedor fornecedor);
	
}
