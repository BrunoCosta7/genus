package model;

public class Endereco {
	
	private int id;
	private String logradouro;
	private int numero;
	private String complemento;
	private String bairro;
	private Cidade cidade;
	private String id_pessoa_cpf_cnpj;
	
	public Endereco(){};
	
	public void setId_pessoa_cpf_cnpj(String id_pessoa_cpf_cnpj) {
		this.id_pessoa_cpf_cnpj = id_pessoa_cpf_cnpj;
	}
	
	public String getId_pessoa_cpf_cnpj() {
		return id_pessoa_cpf_cnpj;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	};
	
	public String getColunas(){
		return "logradouro,numero,id_pessoa_cpf_cnpj,id_usuario,complemento,bairro,id_cidade";
	}
	
}
