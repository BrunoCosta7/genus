package model;

public class Fornecedor extends Pessoa {
	private String nome_fantasia;
	private Boolean ativo;
	
	private Integer id;

	public String getNome_fantasia() {
		return nome_fantasia;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public Boolean getAtivo() {
		return ativo;
	}

	public void setNome_fantasia(String nome_fantasia) {
		this.nome_fantasia = nome_fantasia;
	}

	public Integer getId() {
		return id;
	}
	
	public String getAtivoNome(){
		if(ativo){
			return "Sim";
		}else{
			return "N�o";
		}
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public Fornecedor(){};

}
