package model;

public class Usuario extends Pessoa {
	
	private Integer id;
	private String login;
	private Boolean administrador;
	private String senha;
	private String cpf_cnpj_pessoa;
	private Boolean ativo;
	
	
	public Usuario(){};
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public Boolean getAdministrador() {
		return administrador;
	}
	public void setAdministrador(Boolean administrador) {
		this.administrador = administrador;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public String getColunasFilho(){
		return "login,senha,administrador,ativo,cpf_cnpj_pessoa";
	}

	public String getCpf_cnpj_pessoa() {
		return cpf_cnpj_pessoa;
	}

	public void setCpf_cnpj_pessoa(String cpf_cnpj_pessoa) {
		this.cpf_cnpj_pessoa = cpf_cnpj_pessoa;
	}
	

}
