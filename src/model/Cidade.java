package model;

public class Cidade {

	private int id_cidade;
	private String nome_cidade;
	private Estado estado;
	
	public Cidade(){}

	public int getId_cidade() {
		return id_cidade;
	}

	public void setId_cidade(int id_cidade) {
		this.id_cidade = id_cidade;
	}

	public String getNome_cidade() {
		return nome_cidade;
	}

	public void setNomeCidade(String nome_cidade) {
		this.nome_cidade = nome_cidade;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	};
	
	public String getColunas(){
		return "id_cidade,nome_cidade,id_estado";
	
	}
}
