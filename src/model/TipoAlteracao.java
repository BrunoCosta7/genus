package model;

public class TipoAlteracao {
	
	//N�o mudar id dessas constantes
	public static Integer TIPO_ALTERACAO_INCLUSAO	 	= 1;
	public static Integer TIPO_ALTERACAO_EXCLUSAO	 	= 2;
	
	public static Integer TIPO_ALTERACAO_NOME 			= 3;
	public static Integer TIPO_ALTERACAO_QUANTIDADE 	= 4;
	public static Integer TIPO_ALTERACAO_PRECO_CUSTO 	= 5;
	public static Integer TIPO_ALTERACAO_PRECO_VENDA 	= 6;
	public static Integer TIPO_ALTERACAO_FORNECEDOR 	= 7;
	public static Integer TIPO_ALTERACAO_MEDIDA		 	= 8;
	
	public static Integer TIPO_ACAO_SALVAR 	= 1;
	public static Integer TIPO_ACAO_ALTERAR	= 2;
	public static Integer TIPO_ACAO_EXCLUIR = 3;
	
	private Integer id_tipo_alteracao;
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public Integer getId_tipo_alteracao() {
		return id_tipo_alteracao;
	}
	
	public void setId_tipo_alteracao(Integer id_tipo_alteracao) {
		this.id_tipo_alteracao = id_tipo_alteracao;
	}
}
