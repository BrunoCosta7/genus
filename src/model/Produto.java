package model;

import java.text.NumberFormat;

public class Produto {
	
	private Integer id;
	private String nome;
	private String medida;
	private Integer quantidade;
	private Fornecedor fornecedor;
	private Double precoCusto;
	private Double precoVenda;
	private Boolean ativo;
	
	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getMedida(){
		return medida;
	}
	
	public void setMedida(String medida){
		this.medida = medida;
	}
	
	public Integer getQuantidade(){
		return quantidade;
	}
	
	public void setQuantidade(Integer quantidade){
		this.quantidade = quantidade;
	}
	
	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public void setPrecoCusto(Double precoCusto) {
		this.precoCusto = precoCusto;
	}

	public Double getPrecoCusto(){
		return precoCusto;
	}
	
	public void setPrecoVenda(Double precoVenda){
		this.precoVenda = precoVenda;
	}
	
	public Double getPrecoVenda(){
		return precoVenda;
	}
	
	public Boolean getAtivo() {
		return ativo;
	}
	
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	public String getPrecoVendaFormatado(){
		return NumberFormat.getCurrencyInstance().format(getPrecoVenda());
	}
	
	public String getPrecoCustoFormatado(){
		return NumberFormat.getCurrencyInstance().format(getPrecoCusto());
	}
}

