package model;

import java.util.Date;

import utils.UtilProject;

public class Historico {
	 
	private Integer id_historico;;
	private Produto produto;
	private Usuario usuario;
	private Date 	data_hora;
	private String 	valor_antigo;
	private String 	valor_novo;
	private TipoAlteracao tipo_alteracao;
	
	public Integer getId_historico() {
		return id_historico;
	}
	
	public void setId_historico(Integer id_historico) {
		this.id_historico = id_historico;
	}
	public Date getData_hora() {
		return data_hora;
	}
	public void setData_hora(Date data_hora) {
		this.data_hora = data_hora;
	}
	public String getValor_antigo() {
		return valor_antigo;
	}
	public void setValor_antigo(String valor_antigo) {
		this.valor_antigo = valor_antigo;
	}
	public String getValor_novo() {
		return valor_novo;
	}
	public void setValor_novo(String valor_novo) {
		this.valor_novo = valor_novo;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public TipoAlteracao getTipo_alteracao() {
		return tipo_alteracao;
	}

	public void setTipo_alteracao(TipoAlteracao tipo_alteracao) {
		this.tipo_alteracao = tipo_alteracao;
	}
	
	public String getData_hora_formatada(){
		return UtilProject.formatarData(getData_hora().toString());
	}
}
