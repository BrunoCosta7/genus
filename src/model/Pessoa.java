package model;

public class Pessoa{
	
	private String email;
	private String telefone_numero;
	private String telefone_ddd;
	private String nome;
	private String insc_estadual;
	private Endereco endereco;
	private int id_usuario;
	private int id_fornecedor;
	private String id_pessoa_cpf_cnpj;
	private String cpf_cnpj_aux_alterar;
	
	
	public void setCpf_cnpj_aux_alterar(String cpf_cnpj_aux_alterar) {
		this.cpf_cnpj_aux_alterar = cpf_cnpj_aux_alterar;
	}
	
	public String getCpf_cnpj_aux_alterar() {
		return cpf_cnpj_aux_alterar;
	}
	
	public String getId_pessoa_cpf_cnpj() {
		return id_pessoa_cpf_cnpj;
	}
	public void setId_pessoa_cpf_cnpj(String id_pessoa_cpf_cnpj) {
		this.id_pessoa_cpf_cnpj = id_pessoa_cpf_cnpj;
	}
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public int getId_fornecedor() {
		return id_fornecedor;
	}
	public void setId_fornecedor(int id_fornecedor) {
		this.id_fornecedor = id_fornecedor;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone_numero() {
		return telefone_numero;
	}
	public void setTelefone_numero(String telefone_numero) {
		this.telefone_numero = telefone_numero;
	}
	public String getTelefone_ddd() {
		return telefone_ddd;
	}
	public void setTelefone_ddd(String telefone_ddd) {
		this.telefone_ddd = telefone_ddd;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getInsc_estadual() {
		return insc_estadual;
	}
	public void setInsc_estadual(String insc_estadual) {
		this.insc_estadual = insc_estadual;
	}
	
	public String  getColunasPai(){
		return "cpf_cnpj_pessoa,nome,telefone_ddd,telefone_numero,email,insc_estadual,id_endereco";
	}
	
	public Pessoa getPessoa(){
		Pessoa pessoa = new  Pessoa();
		pessoa.setId_pessoa_cpf_cnpj(getId_pessoa_cpf_cnpj());
		pessoa.setEmail(getEmail());
		pessoa.setEndereco(getEndereco());
		pessoa.setNome(getNome());
		pessoa.setInsc_estadual(getInsc_estadual());
		pessoa.setTelefone_ddd(getTelefone_ddd());
		pessoa.setTelefone_numero(getTelefone_numero());
		pessoa.setId_usuario(getId_usuario());
		pessoa.setCpf_cnpj_aux_alterar(getCpf_cnpj_aux_alterar());
		return pessoa;
		
	}
	

}
