package model;

public class Estado {
	private String id_estado; // SIGLA
	private String nome_estado;
	
	public Estado(){};
	
	
	public String getId_estado() {
		return id_estado;
	}
	public void setId_estado(String id_estado) {
		this.id_estado = id_estado;
	}
	public String getNome_estado() {
		return nome_estado;
	}
	public void setNomeEstado(String nome_estado) {
		this.nome_estado = nome_estado;
	}
	
	public String getColunas(){
		return "id_estado,nome_estado";
	}
	
	

}
