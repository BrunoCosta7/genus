package Controller;

import java.util.Date;
import java.util.List;

import model.Historico;
import model.Produto;
import model.TipoAlteracao;
import utils.Result;
import utils.UtilProject;
import InterfacesController.IProdutoController;
import InterfacesDao.IHistoricoDao;
import InterfacesDao.IProdutoDao;
import Persistencia.HistoricoDao;
import Persistencia.ProdutoDao;

public class ProdutoController implements IProdutoController {
	
	private static ProdutoController produtoController;
	IHistoricoDao historicoDao = HistoricoDao.getInstance();
	IProdutoDao produtoDao = ProdutoDao.getInstance();
	
	public static ProdutoController getInstance(){
		if(produtoController == null){
			produtoController = new ProdutoController();
		}
		return produtoController;
	}
	
	@Override
	public Result salvar(Produto produto) {

		Result r = validarRegrasAntesIncluir(produto);
		
		if(r.isSucesso()){
			r = produtoDao.salvar(produto);
		}
		
		if(r.isSucesso())gerarLogProduto(produto, TipoAlteracao.TIPO_ACAO_SALVAR);
		return r;
	}

	@Override
	public Result excluir(Produto produto) {
		Produto produtoAux = produto;
		Result r = produtoDao.excluir(produto);
		if(r.isSucesso())gerarLogProduto(produtoAux, TipoAlteracao.TIPO_ACAO_EXCLUIR);
		return r;
	}

	@Override
	public Result alterar(Produto produto) {
		Produto produtoAux = pesquisarPorId(produto.getId());
		
		Result r = validarRegrasAntesAlterar(produto);
		
		if(r.isSucesso()){
			r = produtoDao.alterar(produto);
		}
		
		if(r.isSucesso())gerarLogProduto(produtoAux, TipoAlteracao.TIPO_ACAO_ALTERAR);
		return r;
	}

	@Override
	public List<Produto> listObjects() {
		return produtoDao.listObjects();
	}

	@Override
	public Produto pesquisar(Integer id_produto) {
		return produtoDao.pesquisarPorId(id_produto);
	}

	@Override
	public List<Produto> filtrar(String nome, String medida) {
		return produtoDao.filtrar(nome, medida);
	}

	@Override
	public Result validarRegrasAntesIncluir(Produto produto) {
		Result r = new Result();
		r.setSucesso(true);
		
		if(produto.getNome() == null || produto.getNome().equals("")){
			r.setSucesso(false);
			r.addMensagem("O nome do produto � obrigat�rio!");
		}
		
		if(produto.getMedida() == null || produto.getMedida().equals("")){
			r.setSucesso(false);
			r.addMensagem("A medida do produto � obrigat�ria!");
		}
		
		if(produto.getPrecoCusto() == null){
			r.setSucesso(false);
			r.addMensagem("O produto deve conter pre�o de custo!");
		}
		
		if(produto.getPrecoVenda() == null){
			r.setSucesso(false);
			r.addMensagem("O produto deve conter o pre�o de venda!");
		}
		
		if(produto.getPrecoCusto() <= 0){
			r.setSucesso(false);
			r.addMensagem("O valor de pre�o de custo n�o pode ser aceito!");
		}
		
		if(produto.getPrecoVenda() <= produto.getPrecoCusto()){
			r.setSucesso(false);
			r.addMensagem("O valor do pre�o de venda n�o pode ser menor que o valor do pre�o de custo!");
		}
		
		return r;
	}

	@Override
	public Result validarRegrasAntesAlterar(Produto produto) {
		
		Result r = new Result();
		r.setSucesso(true);
		
		if(produto.getNome() == null || produto.getNome().equals("")){
			r.setSucesso(false);
			r.addMensagem("O nome do produto � obrigat�rio!");
			System.out.println("Nome do produto � obrigat�rio");
		}
		
		if(produto.getMedida() == null || produto.getMedida().equals("")){
			r.setSucesso(false);
			r.addMensagem("A medida do produto � obrigat�ria!");
			System.out.println("Medida do produto � obrigat�rio");
		}
		
		if(produto.getPrecoCusto() == null){
			r.setSucesso(false);
			r.addMensagem("O produto deve conter pre�o de custo!");
			System.out.println("Custo do produto � obrigat�rio");
		}
		
		if(produto.getPrecoVenda() == null){
			r.setSucesso(false);
			r.addMensagem("O produto deve conter o pre�o de venda!");
			System.out.println("Venda do produto � obrigat�rio");
		}
		
		if (produto.getPrecoCusto() <= 0){
			r.setSucesso(false);
			r.addMensagem("O valor de pre�o de custo n�o pode ser aceito");
			System.out.println("Custo do produto n�o pode ser aceito");
		}
		
		if(produto.getPrecoVenda() <= produto.getPrecoCusto()){
			r.setSucesso(false);
			r.addMensagem("O valor do pre�o de venda n�o pode ser menor que o valor do pre�o de custo!");
			System.out.println("Venda do produto n�o pode ser aceito");
		}
		
		return r;
	}
	
	@Override
	public Produto pesquisarPorId(Integer id_produto) {
		return produtoDao.pesquisarPorId(id_produto);
	}
	
	private void gerarLogProduto(Produto produto, Integer acao){
		
		if(acao.equals(TipoAlteracao.TIPO_ACAO_SALVAR)){
			
			Historico historico = new Historico();
			historico.setData_hora(new Date());
			historico.setProduto(produto);
			historico.setUsuario(UtilProject.getUsuarioLogado());
			
			TipoAlteracao tipo = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_INCLUSAO);
			historico.setTipo_alteracao(tipo);
			
			historico.setValor_antigo("");
			historico.setValor_novo("");
			historicoDao.salvar(historico);
			
		}else if(acao.equals(TipoAlteracao.TIPO_ACAO_ALTERAR)){
			
			Produto produtoNovo = pesquisarPorId(produto.getId());
			
			if(!produtoNovo.getNome().equals(produto.getNome())){
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_NOME);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getNome());
				historico.setValor_novo(produto.getNome());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
			if(!produtoNovo.getFornecedor().getId().equals(produto.getFornecedor().getId())){
				
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_FORNECEDOR);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getFornecedor().getNome());
				historico.setValor_novo(produto.getFornecedor().getNome());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
			if(!produtoNovo.getMedida().equals(produto.getMedida())){
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_MEDIDA);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getMedida());
				historico.setValor_novo(produto.getMedida());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
			if(!produtoNovo.getPrecoCusto().equals(produto.getPrecoCusto())){
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_PRECO_CUSTO);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getPrecoCusto().toString());
				historico.setValor_novo(produto.getPrecoCusto().toString());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
			if(!produtoNovo.getPrecoVenda().equals(produto.getPrecoVenda())){
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_PRECO_VENDA);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getPrecoVenda().toString());
				historico.setValor_novo(produto.getPrecoVenda().toString());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
			if(!produtoNovo.getQuantidade().equals(produto.getQuantidade())){
				Historico historico = new Historico();
				historico.setData_hora(new Date());
				historico.setProduto(produto);
				
				TipoAlteracao alteracao = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_QUANTIDADE);
				
				historico.setTipo_alteracao(alteracao);
				historico.setValor_antigo(produtoNovo.getQuantidade().toString());
				historico.setValor_novo(produto.getQuantidade().toString());
				historico.setUsuario(UtilProject.getUsuarioLogado());
				
				historicoDao.salvar(historico);
			}
			
		}else if(acao.equals(TipoAlteracao.TIPO_ACAO_EXCLUIR)){
			
			Historico historico = new Historico();
			historico.setData_hora(new Date());
			historico.setProduto(produto);
			historico.setUsuario(UtilProject.getUsuarioLogado());
			
			TipoAlteracao tipo = historicoDao.getTipoAlteracao(TipoAlteracao.TIPO_ALTERACAO_EXCLUSAO);
			historico.setTipo_alteracao(tipo);
			
			historico.setValor_antigo("");
			historico.setValor_novo("");
			historicoDao.salvar(historico);
			
		}
	}
	
}
