package Controller;

import java.util.List;

import InterfacesController.IEnderecoController;
import InterfacesController.IFornecedorController;
import InterfacesDao.IFornecedorDao;
import Persistencia.FornecedorDao;
import model.Fornecedor;
import utils.Result;

public class FornecedorController implements IFornecedorController{

	private static FornecedorController fornecedorController;
	private IFornecedorDao fornecedorDao = FornecedorDao.getInstance();
	private IEnderecoController enderecoControler = EnderecoController.getInstance();
	
	@Override
	public Result salvar(Fornecedor fornecedor) {
		
		Result r = validarRegrasAntesIncluir(fornecedor);
		if(r.isSucesso()){
			return fornecedorDao.salvar(fornecedor);
		}else {
			return r;
		}
	}

	@Override
	public Result excluir(Fornecedor fornecedor) {
		return fornecedorDao.excluir(fornecedor);
	}

	@Override
	public Result alterar(Fornecedor fornecedor) {
		Result r = validarRegrasAntesAlterar(fornecedor);
		if(r.isSucesso()){
			return fornecedorDao.alterar(fornecedor);
		}else{
			return r;
		}
	}

	@Override
	public List<Fornecedor> listObjects() {
		return fornecedorDao.listObjects();
	}

	@Override
	public Fornecedor pesquisar(String cpf_cnpj_id_fornecedor) {
		return fornecedorDao.pesquisarPorCpf(cpf_cnpj_id_fornecedor);
	}

	@Override
	public Fornecedor pesquisarPorNome(String nomeFornecedor) {
		return fornecedorDao.pesquisarPorNomeFantasia(nomeFornecedor);
	}
	
	@Override
	public List<Fornecedor> filtrar(String nome) {
		return fornecedorDao.filtrar(nome);
	}

	@Override
	public Result validarRegrasAntesIncluir(Fornecedor fornecedor) {
		return validacoesComuns(fornecedor);
	}

	private Result validacoesComuns(Fornecedor fornecedor){
		
		Result r = new Result("", true);
		if(fornecedor != null){
			if(fornecedor.getNome() == null || fornecedor.getNome()!=null && fornecedor.getNome().isEmpty()){
				r.setSucesso(false);
				r.addMensagem("O respons�vel do Fornecedor � obrigat�rio");
			}
			
			if(fornecedor.getNome_fantasia() == null || fornecedor.getNome_fantasia()!=null && fornecedor.getNome_fantasia().isEmpty()){
				r.setSucesso(false);
				r.addMensagem("O Nome Fantasia do Fornecedor � obrigat�rio");
			}
			
			if(fornecedor.getTelefone_ddd() == null || fornecedor.getTelefone_ddd()!=null && fornecedor.getTelefone_ddd().isEmpty() || 
				fornecedor.getTelefone_numero() == null || fornecedor.getTelefone_numero()!=null && fornecedor.getTelefone_numero().isEmpty()){
				r.setSucesso(false);
				r.addMensagem("O Telefone do Fornecedor � obrigat�rio");
			}
			
			if(fornecedor.getEmail() == null || fornecedor.getEmail()!=null && fornecedor.getEmail().isEmpty()){
				r.setSucesso(false);
				r.addMensagem("O Email do Fornecedor � obrigat�rio");
			}
			
			
			if(fornecedor.getId_pessoa_cpf_cnpj() == null || fornecedor.getId_pessoa_cpf_cnpj().equals("")){
				r.setSucesso(false);
				r.addMensagem("O CPF/CNPJ do Fornecedor � obrigat�rio");
			}
			
			if(fornecedor.getPessoa().getEndereco() == null){
				r.setSucesso(false);
				r.addMensagem("O Endere�o do Fornecedor � obrigat�rio");
			}else{
				if(fornecedor.getPessoa().getEndereco().getCidade() ==null){
					r.setSucesso(false);
					r.addMensagem("A cidade do Fornecedor � obrigat�rio");
				}else if(fornecedor.getPessoa().getEndereco().getCidade().getEstado() == null){
					r.setSucesso(false);
					r.addMensagem("O Estado do Fornecedor � obrigat�rio");
				}
				
				if(fornecedor.getPessoa().getEndereco().getBairro() == null || fornecedor.getPessoa().getEndereco().getBairro()!=null && fornecedor.getPessoa().getEndereco().getBairro().isEmpty()){
					r.setSucesso(false);
					r.addMensagem("O Bairro do Fornecedor � obrigat�rio");
				}
				
				if(fornecedor.getPessoa().getEndereco().getLogradouro() == null || fornecedor.getPessoa().getEndereco().getLogradouro()!=null && fornecedor.getPessoa().getEndereco().getLogradouro().isEmpty()){
					r.setSucesso(false);
					r.addMensagem("O Logradouro do Fornecedor � obrigat�rio");
				}
				
				
				if(fornecedor.getPessoa().getEndereco().getNumero() == 0){
					r.setSucesso(false);
					r.addMensagem("O N�mero do Fornecedor � obrigat�rio");
				}
			}
			
			if(fornecedor.getPessoa().getEndereco().getCidade() != null && fornecedor.getPessoa().getEndereco().getCidade().getEstado()!=null ){
				Result rEndereco = enderecoControler.verificarEstadoCidade(fornecedor.getPessoa().getEndereco().getCidade());
				if(!rEndereco.isSucesso()){
					r.setSucesso(false);
					r.addMensagem(rEndereco.getMensagem());
				}
			}
		}
		return r;
		
	}
	
	@Override
	public Fornecedor exist(Fornecedor fornecedor){
		return fornecedorDao.exist(fornecedor);
	}
	
	@Override
	public Result validarRegrasAntesAlterar(Fornecedor fornecedor) {
		return validacoesComuns(fornecedor);
	}
	
	public static FornecedorController getInstance(){
		if(fornecedorController==null){
			fornecedorController = new FornecedorController();
		}
		return fornecedorController;
	}
	
	private FornecedorController(){}
}
