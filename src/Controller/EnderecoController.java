package Controller;

import java.util.List;

import model.Cidade;
import model.Endereco;
import model.Estado;
import utils.Result;
import InterfacesController.IEnderecoController;
import InterfacesDao.IEnderecoDao;
import Persistencia.EnderecoDao;

public class EnderecoController implements IEnderecoController{

	private static EnderecoController enderecoController;
	private IEnderecoDao enderecoDAO = EnderecoDao.getInstance();
	
	@Override
	public Result salvar(Endereco endereco) {
		
		validarRegrasAntesIncluir();
		
		return enderecoDAO.salvar(endereco);
	}

	@Override
	public Result excluir(Endereco usuario) {
		return enderecoDAO.excluir(usuario);
	}

	@Override
	public Result alterar(Endereco usuario) {
		validarRegrasAntesAlterar();
		return enderecoDAO.alterar(usuario);
	}

	
	@Override
	public List<Cidade> getCidades(String idEstado) {
		return enderecoDAO.getCidades(idEstado);
	}
	
	@Override
	public Result verificarEstadoCidade(Cidade cidade){
		return enderecoDAO.verificarEstadoCidade(cidade);
	}
	
	
	@Override
	public List<Estado> getEstados() {
		return enderecoDAO.getEstados();
	}
	@Override
	public Endereco pesquisar(String cpf_cnpj_id_usuario) {
		return null;
	}

	@Override
	public List<Endereco> filtrar(String nome) {
		return null;
	}

	@Override
	public void validarRegrasAntesIncluir() {
		// TODO Auto-generated method stub
	}

	@Override
	public void validarRegrasAntesAlterar() {
		// TODO Auto-generated method stub
	}
	
	public static EnderecoController getInstance(){
		if(enderecoController == null){
			enderecoController = new EnderecoController();
		}
		return enderecoController;
	}
	
	private EnderecoController(){}
}
