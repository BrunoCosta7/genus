package Controller;

import java.util.List;

import model.Historico;
import InterfacesController.IHistoricoController;
import InterfacesDao.IHistoricoDao;
import Persistencia.HistoricoDao;

public class HistoricoController implements IHistoricoController{
	
	private static HistoricoController historicoController;
	IHistoricoDao historicoDao = HistoricoDao.getInstance();
	
	@Override
	public List<Historico> listarTodosHistoricos(){
		return historicoDao.listarTodos();
	}
	
	@Override
	public List<Historico> filtrar(String nomeProduto, String dataInicial,String dataFinal) {
		return historicoDao.filtrar(nomeProduto, dataInicial, dataFinal);
	}
	
	public static HistoricoController getInstance(){
		if(historicoController == null){
			historicoController = new HistoricoController();
		}
		return historicoController;
	}
	
}
