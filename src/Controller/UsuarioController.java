package Controller;

import java.util.List;

import model.Usuario;
import utils.Result;
import InterfacesController.IEnderecoController;
import InterfacesController.IUsuarioController;
import InterfacesDao.IUsuarioDao;
import Persistencia.UsuarioDao;

public class UsuarioController implements IUsuarioController{

	private static UsuarioController usuarioController;
	private IUsuarioDao usuarioDao = UsuarioDao.getInstance();
	private IEnderecoController enderecoControler = EnderecoController.getInstance();
	
	@Override
	public Result salvar(Usuario usuario) {
		
		Result r = validarRegrasAntesIncluir(usuario);
		if(r.isSucesso()){
			return usuarioDao.salvar(usuario);
		}else {
			return r;
		}
	}

	@Override
	public Result excluir(Usuario usuario) {
		return usuarioDao.excluir(usuario);
	}

	@Override
	public Result alterar(Usuario usuario) {
		Result r = validarRegrasAntesAlterar(usuario);
		if(r.isSucesso()){
			return usuarioDao.alterar(usuario);
		}else {
			return r;
		}
	}
	
	@Override
	public Usuario pesquisarUsuarioPorId(Usuario usuarioAux){
		return usuarioDao.pesquisarUsuarioPorId(usuarioAux);
	}
	
	@Override
	public List<Usuario> listObjects() {
		return usuarioDao.listObjects();
	}

	@Override
	public Usuario pesquisar(String cpf_cnpj_id_usuario) {
		return usuarioDao.pesquisarPorCpf(cpf_cnpj_id_usuario);
	}

	@Override
	public List<Usuario> filtrar(String nome) {
		return usuarioDao.filtrar(nome);
	}

	@Override
	public Result validarRegrasAntesIncluir(Usuario usuario) {
		Result r = new Result("", true);
		
		if(usuario != null){
			
			if(usuario.getNome() == null || usuario.getNome().equals("")){
				r.setSucesso(false);
				r.addMensagem("O nome do Usu�rio � obrigat�rio");
			}
			
			if(usuario.getId_pessoa_cpf_cnpj() == null || usuario.getId_pessoa_cpf_cnpj().equals("")){
				r.setSucesso(false);
				r.addMensagem("O CPF do Usu�rio � obrigat�rio");
			}
			
			if(usuario.getPessoa().getEndereco().getCidade() != null && usuario.getPessoa().getEndereco().getCidade().getEstado()!=null ){
				Result rEndereco = enderecoControler.verificarEstadoCidade(usuario.getPessoa().getEndereco().getCidade());
				if(!rEndereco.isSucesso()){
					r.setSucesso(false);
					r.addMensagem(rEndereco.getMensagem());
				}
			}
			
		}
		
		return r;
	}

	@Override
	public Result validarRegrasAntesAlterar(Usuario usuario) {
		Result r = new Result("", true);
		if(usuario != null){
			if(usuario.getNome() == null || usuario.getNome().equals("")){
				r.setSucesso(false);
				r.addMensagem("O nome do Usu�rio � obrigat�rio");
			}
			
			if(usuario.getId_pessoa_cpf_cnpj() == null || usuario.getId_pessoa_cpf_cnpj().equals("")){
				r.setSucesso(false);
				r.addMensagem("O CPF do Usu�rio � obrigat�rio");
			}
			
			if(usuario.getPessoa().getEndereco().getCidade() != null && usuario.getPessoa().getEndereco().getCidade().getEstado()!=null ){
				Result rEndereco = enderecoControler.verificarEstadoCidade(usuario.getPessoa().getEndereco().getCidade());
				if(!rEndereco.isSucesso()){
					r.setSucesso(false);
					r.addMensagem(rEndereco.getMensagem());
				}
			}
		}
		return r;
	}
	
	public static UsuarioController getInstance(){
		if(usuarioController==null){
			usuarioController = new UsuarioController();
		}
		return usuarioController;
	}
	
	private UsuarioController(){}
}
