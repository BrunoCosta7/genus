package InterfacesDao;

import java.util.List;

import model.Produto;
import utils.Result;

public interface IProdutoDao {
	public Result salvar(Produto produto);
	public Result excluir(Produto produto);
	public Result alterar(Produto produto);
	public List<Produto> listObjects();
	public Produto pesquisarPorNomeMedida(String nome_medida_produto);
	public Produto pesquisarPorId(Integer id_produto);
	public List<Produto> filtrar(String nome, String medida);
	public boolean exist(Produto produto);
	
}