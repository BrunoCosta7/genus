package InterfacesDao;

import java.util.List;

import model.Fornecedor;
import utils.Result;

public interface IFornecedorDao {

	public Result salvar(Fornecedor fornecedor);
	public Result excluir(Fornecedor fornecedor);
	public Result alterar(Fornecedor fornecedor);
	public List<Fornecedor> listObjects();
	public Fornecedor pesquisarPorCpf(String cpf_cnpj_id_fornecedor);
	public Fornecedor pesquisarPorNomeFantasia(String nome_fantasia);
	public List<Fornecedor> filtrar(String nome);
	public Fornecedor exist(Fornecedor fornecedor);
	
}
