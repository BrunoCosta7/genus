package InterfacesDao;

import java.util.List;

import model.Usuario;
import utils.Result;

public interface IUsuarioDao {

	public Result salvar(Usuario usuario);
	public Result excluir(Usuario usuario);
	public Result alterar(Usuario usuario);
	public List<Usuario> listObjects();
	public Usuario pesquisarPorCpf(String cpf_cnpj_id_usuario);
	public List<Usuario> filtrar(String nome);
	public Boolean exist(Usuario usuario);
	public Usuario pesquisarUsuario(String login);
	public Usuario pesquisarUsuarioPorId(Usuario usuarioAux);
	
}
