package InterfacesDao;

import java.util.List;

import model.Historico;
import model.TipoAlteracao;
import utils.Result;

public interface IHistoricoDao {
	
	public Result salvar(Historico historico);
	public TipoAlteracao getTipoAlteracao(Integer tipoAlteracao);
	public List<Historico> listarTodos();
	public List<Historico> filtrar(String nomeProduto, String datainicial, String dataFinal);
}
