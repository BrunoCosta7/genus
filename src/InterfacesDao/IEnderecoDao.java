package InterfacesDao;

import java.util.List;

import model.Cidade;
import model.Endereco;
import model.Estado;
import utils.Result;

public interface IEnderecoDao {
	
	public Result salvar(Endereco object);
	
	public Result alterar(Endereco object);
	
	public Result verificarEstadoCidade(Cidade cidade);
	
	public Result excluir(Endereco object);
	
	public Endereco pesquisaEnderecoPorPessoa(String id_pessoa_cpf_cnpj);
	
	public int pesquisaEnderecoPorUsuario(int id_usuario);
	
	public List<Cidade> getCidades(String idEstado);
	
	public List<Estado> getEstados();
	
	
}
