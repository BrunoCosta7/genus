package InterfacesDao;

import model.Pessoa;
import utils.Result;

public interface IPessoaDao {
	
	public Result salvar(Pessoa pessoa);
	public Boolean alterar(Pessoa pessoa);
	public Result excluir(Pessoa pessoa);
	public Boolean unicoCnpjExistente(String cpf_cnpj);
	public Pessoa pesquisarCpfCnpj(String cpf_cnpj);
	
}
