package ServletController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Fornecedor;
import model.Produto;
import utils.Acoes;
import utils.Result;
import Controller.FornecedorController;
import Controller.ProdutoController;
import InterfacesController.IFornecedorController;
import InterfacesController.IProdutoController;

@WebServlet("/ProdutoSV")
public class ProdutoSV extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private IProdutoController produtoController;
	private IFornecedorController fornecedorController;
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		produtoController = ProdutoController.getInstance();
		fornecedorController = FornecedorController.getInstance();
		processarAcao(request, response);
	}
	
	private void processarAcao(HttpServletRequest request, HttpServletResponse response){
		String acao = request.getParameter("acao");
		
		if(acao.equals(Acoes.NOVO.getDescricao())){
			novo(request, response);
		}else if(acao.equals(Acoes.SALVAR.getDescricao())){
			salvar(request, response);
		}else if(acao.equals(Acoes.EDITAR.getDescricao())){
			editar(request, response);
		}else if(acao.equals(Acoes.EXCLUIR.getDescricao())){
			excluir(request, response);
		}else if(acao.equals(Acoes.FILTRAR.getDescricao())){
			filtrar(request, response);
		}else {
			voltar(request, response);
		}
	}
	
	private void editar(HttpServletRequest request, HttpServletResponse response) {
		
		String id = request.getParameter("id");
		List<Produto> list = new ArrayList<Produto>();
		list = produtoController.listObjects();
		
		Produto produto = produtoController.pesquisar(Integer.parseInt(id));
		
		if(produto != null){
			
			try {request.setAttribute("listaProduto", list);
			request.setAttribute("produto", produto);
			request.setAttribute("abaSelecionada", "produto");
			request.setAttribute("abrirModal", "true");
			request.setAttribute("modalcu", "Produto");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
	

	private void salvar(HttpServletRequest request, HttpServletResponse response){
		
		String nome = request.getParameter("nome")!=null?request.getParameter("nome"):"";
		String medida = request.getParameter("medida")!=null?request.getParameter("medida"):"";
		String quantidade = request.getParameter("quantidade")!=null?request.getParameter("quantidade"):"";
		String fornecedorNomeFantasia = request.getParameter("fornecedor")!=null?request.getParameter("fornecedor"):"";
		String precoCusto = request.getParameter("precoCusto") !=null?request.getParameter("precoCusto"):"";
		String precoVenda = request.getParameter("precoVenda") !=null?request.getParameter("precoVenda"):"";
		
		Produto produto = new Produto();
		
		if(request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
			produto.setId(Integer.parseInt(request.getParameter("id")));
		}
		
		produto.setNome(nome);
		produto.setMedida(medida);
		produto.setQuantidade(Integer.parseInt(quantidade));
		produto.setPrecoCusto(Double.parseDouble(precoCusto));
		produto.setPrecoVenda(Double.parseDouble(precoVenda));
		//produto.setFornecedor(fornecedor);
		
		
			if(produto.getId() == null){
				List<Produto> list = new ArrayList<Produto>();
				list = produtoController.listObjects();
				request.setAttribute("listaProduto", list);
				request.setAttribute("abaSelecionada", "produto");
				
				Fornecedor fornecedor = fornecedorController.pesquisarPorNome(fornecedorNomeFantasia);
				produto.setFornecedor(fornecedor);
				Result r = produtoController.salvar(produto);
				
				if(r.getMensagens()!=null && r.getMensagens().size()>0){
					String mensagens = "";
					for(String mensage : r.getMensagens()){
						mensagens += mensage+ "<br />";
					}
					mensagens=mensagens.substring(0, mensagens.length());
					request.setAttribute("msg", mensagens);
				}else{
					request.setAttribute("msg", r.getMensagem());
				}
				
				if(r.isSucesso()){
					
					try {
						request.setAttribute("produto", new Produto());
						request.setAttribute("abrirModal", "true");
						request.setAttribute("modalcu", "Produto");
						request.getRequestDispatcher("Index.jsp").forward(request, response);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} 
				
				}else{
					
					Fornecedor fornecedor = fornecedorController.pesquisarPorNome(fornecedorNomeFantasia);
					produto.setFornecedor(fornecedor);
					
					Result r = produtoController.alterar(produto);
					
					if(r.isSucesso()){
						
						List<Produto> list = new ArrayList<Produto>();
						list = produtoController.listObjects();
						
						try {
							request.setAttribute("listaProduto", list);
							request.setAttribute("produto", new Produto());
							request.setAttribute("abrirModal", "false");
							request.setAttribute("modalcu", "Produto");
							request.setAttribute("abaSelecionada", "produto");
							request.getRequestDispatcher("Index.jsp").forward(request, response);
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			}
	
	}
	
	private void novo(HttpServletRequest request, HttpServletResponse response){
		
		List<Produto> list = new ArrayList<Produto>();
		list = produtoController.listObjects();
		
		try {
			request.setAttribute("listaProduto", list);
			request.setAttribute("produto", new Produto());
			request.setAttribute("abaSelecionada", "produto");
			request.setAttribute("abrirModal", "true");
			request.setAttribute("modalcu", "Produto");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void excluir (HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("id");
		Produto produto = produtoController.pesquisar(Integer.parseInt(id));
		
		Result result = produtoController.excluir(produto);
		
		List<Produto> list = new ArrayList<Produto>();
		list = produtoController.listObjects();
		
		try {
			if(list != null && list.size()>0){
				request.setAttribute("listaProduto", list);
			}else{
				request.setAttribute("msgProduto", "Nenhum produto encontrado!");
			}
			
			request.setAttribute("produto", new Produto());
			request.setAttribute("abaSelecionada", "produto");
			request.setAttribute("abrirModal", "false");
			request.setAttribute("msg", result.getMensagem());
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void filtrar(HttpServletRequest request, HttpServletResponse response){
		
		String nome = request.getParameter("nomeProdutoFiltrar");
		String medida = request.getParameter("medidaProdutoFiltrar");
		
		List<Produto> list = new ArrayList<Produto>();
		list = produtoController.filtrar(nome!=null?nome:"", medida!=null?medida:"");
		
		try {
			
			if(list != null && list.size() > 0){
				request.setAttribute("listaProduto", list);
			}else{
				request.setAttribute("msgProduto", "Nenhum produto encontrado!");
			}
			
			request.setAttribute("produto", new Produto());
			request.setAttribute("abaSelecionada", "produto");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void voltar(HttpServletRequest request, HttpServletResponse response){
		try {
			request.setAttribute("listaProduto", produtoController.listObjects());
			request.setAttribute("abaSelecionada", "produto");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
