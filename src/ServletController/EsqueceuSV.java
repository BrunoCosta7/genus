package ServletController;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import InterfacesDao.IUsuarioDao;
import Persistencia.UsuarioDao;
import model.Usuario;
import utils.UtilProject;

public class EsqueceuSV  extends HttpServlet{
	IUsuarioDao usuarioDao = UsuarioDao.getInstance();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("/manterLogin/esqueceu.jsp").forward(request, response);
	}
	
	  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
		  String username_nome = request.getParameter("username");
		  String cpf = request.getParameter("password");
		  
		  if(username_nome.isEmpty()||cpf.isEmpty()) {
			  camposBranco(request, response);
		  }
		  
		  Usuario usuario = new Usuario();
		  usuario.setLogin(request.getParameter("username"));
		  usuario.setCpf_cnpj_pessoa(request.getParameter("password"));
		  String cpf_uso = request.getParameter("password");
		  
		  System.out.println("usuario " + usuario.getLogin() + " senha " + usuario.getCpf_cnpj_pessoa());
		  
		 usuario = iniciaProcesso(usuario);
		 System.out.println(usuario.getCpf_cnpj_pessoa() + " cpf0 " + cpf);
		 
		 continuaCadastro(request, response, usuario,cpf_uso);
	  }
	  
	  private Usuario iniciaProcesso(Usuario usuario){
		  Usuario  usuarioAux = usuarioDao.pesquisarUsuario(usuario.getLogin());
		  System.out.println(usuario.getCpf_cnpj_pessoa() + " cpf1 ");
		  if(usuarioAux == null ){
			  usuarioAux = new Usuario();
			  usuarioAux.setId(-1);
		  }
		  boolean exist = usuarioDao.exist(usuarioAux);
		  
		  if (exist) {
			  return usuarioAux;
		  } else {
			  usuarioAux.setLogin("sem login");
			  return usuarioAux;
		  }
	  }
	  
	  private void continuaCadastro(HttpServletRequest request, HttpServletResponse
			  response, Usuario usuario, String cpf) throws ServletException, IOException{
		  if(usuario.getLogin().equals("sem login")){
			  semcadastro(request, response);
		  } else {
			  System.out.println(usuario.getCpf_cnpj_pessoa() + " cpf1 " + cpf);
			  if(usuario.getCpf_cnpj_pessoa().equals(cpf)){
				  cpfCorreto(request, response,usuario.getSenha());
			  } else {
				  retornaCpfErrado(request, response);
			  }
		  }
	  }
	  
	  private void semcadastro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  RequestDispatcher rd = null;
		  request.setAttribute("msgLogin", "Ops! Este Usu�rio n�o existe! Voc� digitou corretamente?");
		  request.setAttribute("msgFClass", "col s12 m12");
		  request.setAttribute("msgSClass", "card red darken-1");
		  rd = request.getRequestDispatcher("/manterLogin/esqueceu.jsp");
		  rd.forward(request, response);
	  }
	  
	  private void cpfCorreto(HttpServletRequest request, HttpServletResponse response, String senha) throws ServletException, IOException{
		  RequestDispatcher rd = null;
		  request.setAttribute("msgLogin", "Sua senha �: " + senha);
		  request.setAttribute("msgFClass", "col s12 m12");
		  request.setAttribute("msgSClass", "card green darken-1");
		  rd = request.getRequestDispatcher("/manterLogin/esqueceu.jsp");
		  rd.forward(request, response);
	  }
	  
	  private void retornaCpfErrado(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		  RequestDispatcher rd = null;
		  request.setAttribute("msgLogin", "Ops! Este cpf est� incorreto! Tente novamente");
		  request.setAttribute("msgFClass", "col s12 m12");
		  request.setAttribute("msgSClass", "card red darken-1");
		  rd = request.getRequestDispatcher("/manterLogin/esqueceu.jsp");
		  rd.forward(request, response);
	  }
	  
	  private void camposBranco(HttpServletRequest request, HttpServletResponse
			  response) throws ServletException, IOException{
		  RequestDispatcher rd = null;
		  request.setAttribute("msgLogin", "Ops!Acho que voc� esqueceu de preencher um dos campos");
		  request.setAttribute("msgFClass", "col s12 m12");
		  request.setAttribute("msgSClass", "card red darken-1");
		  rd = request.getRequestDispatcher("/manterLogin/esqueceu.jsp");
		  rd.forward(request, response);
	  }
	  
		public void doSession(HttpServletRequest request, Usuario usuario){
				HttpSession session = request.getSession();
				session = request.getSession(true);
		        session.setAttribute("usuario", usuario);
		        UtilProject.setUsuarioLogado(usuario);
		        
		        Usuario userTeste = (Usuario) request.getSession().getAttribute("usuario");
		        System.out.println(userTeste.getNome() + userTeste.getAdministrador() + userTeste.getPessoa());
		}
}
