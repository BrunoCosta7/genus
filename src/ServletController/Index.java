<<<<<<< HEAD
package ServletController;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cidade;
import model.Endereco;
import model.Estado;
import model.Fornecedor;
import model.Historico;
import model.Produto;
import model.Usuario;
import utils.UtilProject;
import Controller.EnderecoController;
import Controller.FornecedorController;
import Controller.HistoricoController;
import Controller.ProdutoController;
import Controller.UsuarioController;
import InterfacesController.IEnderecoController;
import InterfacesController.IFornecedorController;
import InterfacesController.IHistoricoController;
import InterfacesController.IProdutoController;
import InterfacesController.IUsuarioController;

//@WebServlet("/index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IUsuarioController 	usuarioController;
	private IEnderecoController enderecoController;
	private IProdutoController 	produtoController;
	private IHistoricoController historicoController;
	private IFornecedorController fornecedorController;
	
	public Index() {
        super();
    }

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Listar todos caso de uso
		listarUsuario(request);
		listarEstados(request);
		listarCidades(request);
		listarProdutos(request);
		listarFornecedores(request);
		
		if(UtilProject.getUsuarioLogado() != null && UtilProject.getUsuarioLogado().getAdministrador()){
			listarHistoricos(request);
		}
		
		try {
			prepararUsuario(request);
			
			request.setAttribute("abaSelecionada", "home");
			request.setAttribute("abrirModal", "false");
//			request.setAttribute("msg", "''");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void listarUsuario(HttpServletRequest request){
		
		usuarioController = UsuarioController.getInstance();
		
		List<Usuario> listaUsuario = usuarioController.listObjects();
		
		if(listaUsuario != null && listaUsuario.size()>0){
			request.setAttribute("listaUsuario", listaUsuario);
		}else{
			request.setAttribute("msgUsuario", "Nenhum usu�rio encontrado");
		}
	}
	
	private void listarEstados(HttpServletRequest request){
		
		enderecoController = EnderecoController.getInstance();
		List<Estado> listaEstados = enderecoController.getEstados();
		
		if(listaEstados != null && listaEstados.size()>0){
			request.setAttribute("listaEstados", listaEstados);
		}
	}
	
	private void listarCidades(HttpServletRequest request){
		
		enderecoController = EnderecoController.getInstance();
		List<Cidade> listaCidades = enderecoController.getCidades("GO");
		
		if(listaCidades != null && listaCidades.size()>0){
			request.setAttribute("listaCidades", listaCidades);
		}
	}
	
	private void prepararUsuario(HttpServletRequest request){
		
		Usuario usuario = new Usuario();
		usuario.setEndereco(new Endereco());
		usuario.setAdministrador(false);
		usuario.setAtivo(false);
		
		request.setAttribute("usuario", usuario);
	}

	private void listarHistoricos(HttpServletRequest request){
		historicoController = HistoricoController.getInstance();
		List<Historico> listaHistorico = historicoController.listarTodosHistoricos();
		
		if(listaHistorico != null && listaHistorico.size()>0){
			request.setAttribute("listaHistorico", listaHistorico);
		}
	}
	
	private void listarProdutos(HttpServletRequest request){
		produtoController = ProdutoController.getInstance();
		List<Produto> listaProduto = produtoController.listObjects();
		if(listaProduto != null && listaProduto.size() > 0){
			request.setAttribute("listaProduto", listaProduto);
		}
	}
	
	private void listarFornecedores(HttpServletRequest request){
		fornecedorController = FornecedorController.getInstance();
		List<Fornecedor> listaFornecedor = fornecedorController.listObjects();
		if(listaFornecedor != null && listaFornecedor.size() > 0){
			request.setAttribute("listaFornecedor", listaFornecedor);
		}
	}
	
}
=======
package ServletController;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.UtilProject;
import model.Cidade;
import model.Endereco;
import model.Estado;
import model.Historico;
import model.Usuario;
import Controller.EnderecoController;
import Controller.ProdutoController;
import Controller.UsuarioController;
import InterfacesController.IEnderecoController;
import InterfacesController.IProdutoController;
import InterfacesController.IUsuarioController;


//@WebServlet("/")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IUsuarioController 	usuarioController;
	private IEnderecoController enderecoController;
	private IProdutoController 	produtoController;
	
	public Index() {
        super();
    }

	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Listar todos caso de uso
		listarUsuario(request);
		listarEstados(request);
		listarCidades(request);
		
		if(UtilProject.getUsuarioLogado().getAdministrador()){
			listarHistoricos(request);
		}
		
		try {
			prepararUsuario(request);
			
			request.setAttribute("abaSelecionada", "home");
			request.setAttribute("abrirModal", "false");
//			request.setAttribute("msg", "''");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void listarUsuario(HttpServletRequest request){
		
		usuarioController = UsuarioController.getInstance();
		
		List<Usuario> listaUsuario = usuarioController.listObjects();
		
		if(listaUsuario != null && listaUsuario.size()>0){
			request.setAttribute("listaUsuario", listaUsuario);
		}else{
			request.setAttribute("msgUsuario", "Nenhum usu�rio encontrado");
		}
	}
	
	private void listarEstados(HttpServletRequest request){
		
		enderecoController = EnderecoController.getInstance();
		List<Estado> listaEstados = enderecoController.getEstados();
		
		if(listaEstados != null && listaEstados.size()>0){
			request.setAttribute("listaEstados", listaEstados);
		}
	}
	
	private void listarCidades(HttpServletRequest request){
		
		enderecoController = EnderecoController.getInstance();
		List<Cidade> listaCidades = enderecoController.getCidades("GO");
		
		if(listaCidades != null && listaCidades.size()>0){
			request.setAttribute("listaCidades", listaCidades);
		}
	}
	
	private void prepararUsuario(HttpServletRequest request){
		
		Usuario usuario = new Usuario();
		usuario.setEndereco(new Endereco());
		usuario.setAdministrador(false);
		usuario.setAtivo(false);
		
		request.setAttribute("usuario", usuario);
	}

	private void listarHistoricos(HttpServletRequest request){
		produtoController = ProdutoController.getInstance();
		List<Historico> listaHistorico = produtoController.listarTodosHistoricos();
		
		if(listaHistorico != null && listaHistorico.size()>0){
			request.setAttribute("listaHistorico", listaHistorico);
		}
	}
	
}
>>>>>>> CasoDeUso-Relatorio
