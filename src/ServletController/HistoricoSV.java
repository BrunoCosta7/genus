package ServletController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Historico;
import utils.Acoes;
import Controller.HistoricoController;
import InterfacesController.IHistoricoController;

/**
 * Servlet implementation class HistoricoSV
 */
@WebServlet("/HistoricoSV")
public class HistoricoSV extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static IHistoricoController historicoController = HistoricoController.getInstance();
    public HistoricoSV() {}
    
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String acao = request.getParameter("acao");
		if(acao != null && acao.equals(Acoes.FILTRAR.getDescricao())){
			filtrar(request, response);
		}
	}
	
	private void filtrar(HttpServletRequest request, HttpServletResponse response){
		String nomeProduto 	= request.getParameter("nomeProduto");
		String dataInicial 	= request.getParameter("dataInicial");
		String dataFinal 	= request.getParameter("dataFinal");
		
		List<Historico> listaHistorico = historicoController.filtrar(nomeProduto, dataInicial, dataFinal);
		
		if(listaHistorico != null && listaHistorico.size() > 0){
			try {
				request.setAttribute("listaHistorico", listaHistorico);
				request.setAttribute("abaSelecionada", "historicoProduto");
				request.setAttribute("abrirModal", "false");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			try {
				request.setAttribute("listaHistorico", new ArrayList<>());
				request.setAttribute("abaSelecionada", "historicoProduto");
				request.setAttribute("abrirModal", "false");
				request.setAttribute("msg", "Nenhum histórico encontrado com esse filtro");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
