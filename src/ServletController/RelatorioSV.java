package ServletController;



import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utils.UtilProject;
import model.Usuario;
import InterfacesDao.IUsuarioDao;
import Persistencia.UsuarioDao;

@WebServlet("/RelatorioSV")
public class RelatorioSV extends HttpServlet{
    private static final long serialVersionUID = 1L;
    IUsuarioDao usuarioDao = UsuarioDao.getInstance();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String tipoReport = request.getParameter("tipoRelatorio");

        PrintWriter out = response.getWriter();

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        out.write("{\"test\": \"recebemos a requisicao do tipo" + tipoReport + "\"}");
        out.flush();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }
}
