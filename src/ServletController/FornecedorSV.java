package ServletController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Controller.FornecedorController;
import InterfacesController.IFornecedorController;
import model.Cidade;
import model.Endereco;
import model.Estado;
import model.Fornecedor;
import utils.Acoes;
import utils.Result;

@WebServlet("/FornecedorSV")
public class FornecedorSV extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private IFornecedorController fornecedorController;
	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		fornecedorController = FornecedorController.getInstance();
		processarAcao(request, response);
		
	}
	
	private void processarAcao(HttpServletRequest request, HttpServletResponse response){
		String acao = request.getParameter("acao");
		
		if(acao.equals(Acoes.NOVO.getDescricao())){
			novo(request, response);
		}else if(acao.equals(Acoes.SALVAR.getDescricao())){
			salvar(request, response);
		}else if(acao.equals(Acoes.EDITAR.getDescricao())){
			editar(request, response);
		}else if(acao.equals(Acoes.EXCLUIR.getDescricao())){
			excluir(request, response);
		}else if(acao.equals(Acoes.FILTRAR.getDescricao())){
			filtrar(request, response);
		}else {
			voltar(request, response);
		}
	}
	
	private void salvar(HttpServletRequest request, HttpServletResponse response){
		
		
		String cpfCnpj = request.getParameter("cpfCnpj")!=null?request.getParameter("cpfCnpj"):"";
		String telefone = request.getParameter("telefone")!=null?request.getParameter("telefone"):"";
		String email = request.getParameter("email")!=null?request.getParameter("email"):"";
		String nome_fantasia = request.getParameter("nome_fantasia")!=null?request.getParameter("nome_fantasia"):"";
		String responsavel = request.getParameter("responsavel")!=null?request.getParameter("responsavel"):"";
		String inscEstadual = request.getParameter("inscricaoEstadual")!=null?request.getParameter("inscricaoEstadual"):"";
		
		boolean isAtivo = request.getParameter("ativo")!=null?true:false;
		
		
		Fornecedor fornecedor = new Fornecedor();
		
		fornecedor.setInsc_estadual(inscEstadual);
		if(request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
			fornecedor.setId(Integer.parseInt(request.getParameter("id")));
		}
		
		fornecedor.setAtivo(isAtivo);
		
		fornecedor.setNome(responsavel);
		fornecedor.setId_pessoa_cpf_cnpj(cpfCnpj);
		fornecedor.setTelefone_ddd(telefone!=""?telefone.substring(0, 2):"");
		fornecedor.setTelefone_numero(telefone!=""?telefone.substring(2, telefone.length()):"");
		fornecedor.setEmail(email);
		fornecedor.setNome_fantasia(nome_fantasia);
	
		
		
		String enderecoLogradouro = request.getParameter("enderecoLogradouro");
		String enderecoNumero = request.getParameter("enderecoNumero");
		String enderecoBairro = request.getParameter("enderecoBairro");
		String enderecoCidade = request.getParameter("enderecoCidade");
		String enderecoEstado = request.getParameter("enderecoEstado");
		String enderecoComplemento = request.getParameter("enderecoComplemento");
		
		Cidade cidade = new Cidade();
		cidade.setNomeCidade(enderecoCidade);
		
		Estado estado = new Estado();
		estado.setNomeEstado(enderecoEstado);
		cidade.setEstado(estado);
		
		Endereco endereco = new Endereco();
		
		endereco.setLogradouro(enderecoLogradouro);
		endereco.setNumero(Integer.parseInt(enderecoNumero!=null&&!enderecoNumero.equals("")?enderecoNumero:"0"));
		endereco.setBairro(enderecoBairro);
		endereco.setCidade(cidade);
		endereco.setComplemento(enderecoComplemento);
		endereco.setId_pessoa_cpf_cnpj(cpfCnpj);
		
		fornecedor.setEndereco(endereco);
		
		if(fornecedor.getId() == null){
			Result r = fornecedorController.salvar(fornecedor);
			
			List<Fornecedor> list = new ArrayList<Fornecedor>();
			list = fornecedorController.listObjects();
			request.setAttribute("listaFornecedor", list);
			request.setAttribute("abaSelecionada", "fornecedor");
			
			if(r.getMensagens()!=null && r.getMensagens().size()>0){
				String mensagens = "";
				for(String mensage : r.getMensagens()){
					mensagens += mensage+"<br />";
				}
				mensagens = mensagens.substring(0, mensagens.length());
				request.setAttribute("msg", mensagens);
			}else{
				request.setAttribute("msg", r.getMensagem());
			}
			
			if(r.isSucesso()){
				
				try {
					request.setAttribute("fornecedor", new Fornecedor());
					request.setAttribute("abrirModal", "false");
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}else{
				
				try {
					request.setAttribute("fornecedor", fornecedor);
					request.setAttribute("abrirModal", "true");
					request.setAttribute("modalcu", "Fornecedor");
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}else{
			Fornecedor teste = new Fornecedor();
			teste.setId(Integer.parseInt(request.getParameter("id")));
			Fornecedor fornecedorTeste = fornecedorController.exist(teste);
			
			fornecedor.setCpf_cnpj_aux_alterar(fornecedorTeste.getPessoa().getId_pessoa_cpf_cnpj());
			Result r = fornecedorController.alterar(fornecedor);
			
			if(r.getMensagens()!=null && r.getMensagens().size()>0){
				String mensagens = "";
				for(String mensage : r.getMensagens()){
					mensagens += mensage+"<br />";
				}
				mensagens = mensagens.substring(0, mensagens.length());
				request.setAttribute("msg", mensagens);
			}else{
				request.setAttribute("msg", r.getMensagem());
			}
			
			List<Fornecedor> list = new ArrayList<Fornecedor>();
			list = fornecedorController.listObjects();
			request.setAttribute("listaFornecedor", list);
			request.setAttribute("abaSelecionada", "fornecedor");
			
			if(r.isSucesso()){
				try {
					request.setAttribute("fornecedor", new Fornecedor());
					request.setAttribute("abrirModal", "false");
					request.setAttribute("msg", r.getMensagem());
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				try {
					request.setAttribute("fornecedor", fornecedor);
					request.setAttribute("abrirModal", "true");
					request.setAttribute("modalcu", "Fornecedor");
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private void editar(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("id");
		List<Fornecedor> list = new ArrayList<Fornecedor>();
		list = fornecedorController.listObjects();
		
		Fornecedor fornecedor = fornecedorController.pesquisar(id);
		
		if(fornecedor != null){
			
			try {
				
				if(fornecedor.getAtivo()){
					request.setAttribute("ativo", "checked");
				}
				request.setAttribute("tituloPagina","Editando fornecedor");
				request.setAttribute("listaFornecedor", list);
				request.setAttribute("fornecedor", fornecedor);
				request.setAttribute("abaSelecionada", "fornecedor");
				request.setAttribute("abrirModal", "true");
				request.setAttribute("modalcu", "Fornecedor");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void novo(HttpServletRequest request, HttpServletResponse response){
		
		List<Fornecedor> list = new ArrayList<Fornecedor>();
		list = fornecedorController.listObjects();
		
			try {
				
				request.setAttribute("ativo", "checked");
				request.setAttribute("tituloPagina","Criando Novo fornecedor");
				request.setAttribute("listaFornecedor", list);
				request.setAttribute("fornecedor", new Fornecedor());
				request.setAttribute("abaSelecionada", "fornecedor");
				request.setAttribute("abrirModal", "true");
				request.setAttribute("modalcu", "Fornecedor");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}
	
	private void excluir(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("id");
		Fornecedor fornecedor = fornecedorController.pesquisar(id);
		
		Result result = fornecedorController.excluir(fornecedor);
		
		List<Fornecedor> list = new ArrayList<Fornecedor>();
		list = fornecedorController.listObjects();
		
		try {
			
			if(list != null && list.size()>0){
				request.setAttribute("listaFornecedor", list);
			}else{
				request.setAttribute("msgFornecedor", "Nenhum fornecedor n�o encontrado");
			}
			
			request.setAttribute("fornecedor", new Fornecedor());
			request.setAttribute("abaSelecionada", "fornecedor");
			request.setAttribute("abrirModal", "false");
			request.setAttribute("msg", result.getMensagem());
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	private void filtrar(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("nomeFiltrar");
		List<Fornecedor> list = new ArrayList<Fornecedor>();
		list = fornecedorController.filtrar(id);
		
		try {
			
			if(list != null && list.size()>0){
				request.setAttribute("listaFornecedor", list);
			}else{
				request.setAttribute("msgFornecedor", "Nenhum fornecedor n�o encontrado");
			}
			
			request.setAttribute("fornecedor", new Fornecedor());
			request.setAttribute("abaSelecionada", "fornecedor");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	private void voltar(HttpServletRequest request, HttpServletResponse response){
		try {
			request.setAttribute("listaFornecedor", fornecedorController.listObjects());
			request.setAttribute("abaSelecionada", "fornecedor");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
