package ServletController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Cidade;
import model.Endereco;
import model.Estado;
import model.Usuario;
import utils.Acoes;
import utils.Result;
import Controller.UsuarioController;
import InterfacesController.IUsuarioController;

@WebServlet("/UsuarioSV")
public class UsuarioSV extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private IUsuarioController usuarioController;
	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		usuarioController = UsuarioController.getInstance();
		processarAcao(request, response);
		
	}
	
	private void processarAcao(HttpServletRequest request, HttpServletResponse response){
		String acao = request.getParameter("acao");
		
		if(acao.equals(Acoes.NOVO.getDescricao())){
			novo(request, response);
		}else if(acao.equals(Acoes.SALVAR.getDescricao())){
			salvar(request, response);
		}else if(acao.equals(Acoes.EDITAR.getDescricao())){
			editar(request, response);
		}else if(acao.equals(Acoes.EXCLUIR.getDescricao())){
			excluir(request, response);
		}else if(acao.equals(Acoes.FILTRAR.getDescricao())){
			filtrar(request, response);
		}else {
			voltar(request, response);
		}
	}
	
	private void salvar(HttpServletRequest request, HttpServletResponse response){
		
		
		String nome = request.getParameter("nome")!=null?request.getParameter("nome"):"";
		String cpfCnpj = request.getParameter("cpfCnpj")!=null?request.getParameter("cpfCnpj"):"";
		String telefone = request.getParameter("telefone")!=null?request.getParameter("telefone"):"";
		String email = request.getParameter("email")!=null?request.getParameter("email"):"";
		String login = request.getParameter("login")!=null?request.getParameter("login"):"";
		String senha = request.getParameter("senha")!=null?request.getParameter("senha"):"";
		boolean isAdministrador = request.getParameter("administrador")!=null?true:false;
		boolean isAtivo = request.getParameter("ativo")!=null?true:false;
		
		Usuario usuario = new Usuario();
		
		if(request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
			usuario.setId(Integer.parseInt(request.getParameter("id")));
		}
		
		usuario.setNome(nome);
		usuario.setId_pessoa_cpf_cnpj(cpfCnpj);
		usuario.setTelefone_ddd(telefone!=""?telefone.substring(0, 2):"");
		usuario.setTelefone_numero(telefone!=""?telefone.substring(2, telefone.length()):"");
		usuario.setEmail(email);
		usuario.setLogin(login);
		usuario.setSenha(senha);
		usuario.setAdministrador(isAdministrador);
		usuario.setAtivo(isAtivo);
	
		
		
		String enderecoLogradouro = request.getParameter("enderecoLogradouro");
		String enderecoNumero = request.getParameter("enderecoNumero");
		String enderecoBairro = request.getParameter("enderecoBairro");
		String enderecoCidade = request.getParameter("enderecoCidade");
		String enderecoEstado = request.getParameter("enderecoEstado");
		String enderecoComplemento = request.getParameter("enderecoComplemento");
		Cidade cidade = new Cidade();
		cidade.setNomeCidade(enderecoCidade);
		
		Estado estado = new Estado();
		estado.setNomeEstado(enderecoEstado);
		cidade.setEstado(estado);
		
		Endereco endereco = new Endereco();
		
		endereco.setLogradouro(enderecoLogradouro);
		endereco.setNumero(Integer.parseInt(enderecoNumero!=null&&!enderecoNumero.equals("")?enderecoNumero:"0"));
		endereco.setBairro(enderecoBairro);
		endereco.setCidade(cidade);
		endereco.setComplemento(enderecoComplemento);
		endereco.setId_pessoa_cpf_cnpj(cpfCnpj);
		usuario.setEndereco(endereco);
		
		if(usuario.getId() == null){
			List<Usuario> list = new ArrayList<Usuario>();
			list = usuarioController.listObjects();
			request.setAttribute("listaUsuario", list);
			request.setAttribute("abaSelecionada", "usuario");
			
			Result r = usuarioController.salvar(usuario);
			
			if(r.getMensagens()!=null && r.getMensagens().size()>0){
				String mensagens = "";
				for(String mensage : r.getMensagens()){
					mensagens += mensage+"<br />";
				}
				mensagens = mensagens.substring(0, mensagens.length());
				request.setAttribute("msg", mensagens);
			}else{
				request.setAttribute("msg", r.getMensagem());
			}
			
			if(r.isSucesso()){
				
				try {
					request.setAttribute("usuario", new Usuario());
					request.setAttribute("abrirModal", "false");
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}else{
				
				try {
					request.setAttribute("usuario", usuario);
					request.setAttribute("abrirModal", "true");
					request.setAttribute("modalcu", "Usuario");
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		}else{
			Usuario usuarioTeste = new Usuario();
			usuarioTeste.setId(Integer.parseInt(request.getParameter("id")));
			Usuario usuarioAux = usuarioController.pesquisarUsuarioPorId(usuarioTeste);
			usuario.setCpf_cnpj_aux_alterar(usuarioAux.getPessoa().getId_pessoa_cpf_cnpj());
			Result r = usuarioController.alterar(usuario);
			
			if(r.isSucesso()){
				
				List<Usuario> list = new ArrayList<Usuario>();
				list = usuarioController.listObjects();
				
				try {
					request.setAttribute("listaUsuario", list);
					request.setAttribute("abaSelecionada", "usuario");
					request.setAttribute("usuario", new Usuario());
					request.setAttribute("abrirModal", "false");
					request.setAttribute("msg", r.getMensagem());
					request.getRequestDispatcher("Index.jsp").forward(request, response);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private void editar(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("id");
		List<Usuario> list = new ArrayList<Usuario>();
		list = usuarioController.listObjects();
		
		Usuario usuario = usuarioController.pesquisar(id);
		
		if(usuario != null){
			
			try {
				
				if(usuario.getAdministrador()){
					request.setAttribute("administrador", "checked");
				}
				
				if(usuario.getAtivo()){
					request.setAttribute("ativo", "checked");
				}
				
				request.setAttribute("listaUsuario", list);
				request.setAttribute("usuario", usuario);
				request.setAttribute("abaSelecionada", "usuario");
				request.setAttribute("abrirModal", "true");
				request.setAttribute("modalcu", "Usuario");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void novo(HttpServletRequest request, HttpServletResponse response){
		
		List<Usuario> list = new ArrayList<Usuario>();
		list = usuarioController.listObjects();
		
			try {
				
				request.setAttribute("listaUsuario", list);
				request.setAttribute("usuario", new Usuario());
				request.setAttribute("abaSelecionada", "usuario");
				request.setAttribute("abrirModal", "true");
				request.setAttribute("modalcu", "Usuario");
				request.getRequestDispatcher("Index.jsp").forward(request, response);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}
	
	private void excluir(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("id");
		Usuario usuario = usuarioController.pesquisar(id);
		
		Result result = usuarioController.excluir(usuario);
		
		List<Usuario> list = new ArrayList<Usuario>();
		list = usuarioController.listObjects();
		
		try {
			
			if(list != null && list.size()>0){
				request.setAttribute("listaUsuario", list);
			}else{
				request.setAttribute("msgUsuario", "Nenhum usu�rio n�o encontrado");
			}
			
			request.setAttribute("usuario", new Usuario());
			request.setAttribute("abaSelecionada", "usuario");
			request.setAttribute("abrirModal", "false");
			request.setAttribute("msg", result.getMensagem());
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	private void filtrar(HttpServletRequest request, HttpServletResponse response){
		
		String id = request.getParameter("nomeFiltrar");
		List<Usuario> list = new ArrayList<Usuario>();
		list = usuarioController.filtrar(id);
		
		try {
			
			if(list != null && list.size()>0){
				request.setAttribute("listaUsuario", list);
			}else{
				request.setAttribute("msgUsuario", "Nenhum usu�rio n�o encontrado");
			}
			
			request.setAttribute("usuario", new Usuario());
			request.setAttribute("abaSelecionada", "usuario");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}
	
	private void voltar(HttpServletRequest request, HttpServletResponse response){
		try {
			request.setAttribute("listaUsuario", usuarioController.listObjects());
			request.setAttribute("abaSelecionada", "usuario");
			request.setAttribute("abrirModal", "false");
			request.getRequestDispatcher("Index.jsp").forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
