package utils;

import java.util.ArrayList;
import java.util.List;

public class Result {
	
	private String mensagem;
	private List<String> mensagens;
	private boolean sucesso;
	
	public Result(String msg, boolean sucesso) {
		setMensagem(msg);
		setSucesso(sucesso);
	}
	
	public Result() {}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public void setSucesso(boolean sucesso) {
		this.sucesso = sucesso;
	}
	
	public boolean isSucesso() {
		return sucesso;
	}
	
	public void setMensagens(List<String> mensagens) {
		this.mensagens = mensagens;
	}
	
	public List<String> getMensagens() {
		return mensagens;
	}
	
	public void addMensagem(String mensagem){
		if(mensagens == null){
			mensagens = new ArrayList<String>();
		}
		mensagens.add(mensagem);
	}
}
