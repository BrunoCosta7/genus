package utils;

import model.Usuario;

/*
 * Todo m�todo que seja �til e que vai ser usados em v�rios lugares
 * declarar ele nessa classe como statico para que n�o seja preciso
 * instanciar a classe
 * */
public class UtilProject {
	
	private static Usuario usuarioLogado;
	
	public static String prepararColunas(String colunas){
		String[] col = colunas.split(",");
		String colunaPronta="";
		for (int i = 0; i < col.length; i++) {
			colunaPronta +=",\""+col[i]+"\"";
		}
		
		return colunaPronta.substring(1);
	}
	
	
	public static String formatarData(String data){
		
		String retorno = "";
		
		String ano = data.substring(0, 4);
		String mes = data.substring(5, 7);
		String dia = data.substring(8, 10);
		
		retorno = dia+"-"+mes+"-"+ano;
		
		if(data.length() > 10){
			String hrs = data.substring(10, 19);
			retorno += hrs;
		}else{
			retorno += " 00:00:00";
		}
		
		return retorno;
	}
	
	public static String formatarDataTimestamp(String data){
		
		String retorno = "";
		if(data.length() > 10){
			String hrs = data.substring(10, 19);
			retorno = data + hrs;
		}else{
			retorno = data+ " 00:00:00";
		}
		
		return retorno;
	}
	
	public static Usuario getUsuarioLogado(){
		return usuarioLogado;
	}
	
	public static void setUsuarioLogado(Usuario usuario){
		usuarioLogado = usuario;
	}
}
