package utils;

public enum Acoes {
	
	SALVAR  ("salvar"),
	EDITAR  ("editar"),
	NOVO    ("novo"),
	EXCLUIR ("excluir"),
	FILTRAR ("filtrar");
	
	
	Acoes(String descricao){
		this.descricao = descricao;
	}
	
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}
}
