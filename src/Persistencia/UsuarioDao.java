package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import model.Endereco;
import model.Pessoa;
import model.Usuario;
import utils.Result;
import InterfacesDao.IEnderecoDao;
import InterfacesDao.IPessoaDao;
import InterfacesDao.IUsuarioDao;

public class UsuarioDao extends ComunDao implements IUsuarioDao{
	
	private static IEnderecoDao enderecoDao;
	private static IPessoaDao pessoaDAO;
	
	private static UsuarioDao dao = null;
	
	public static UsuarioDao getInstance(){
		if(dao==null){
			dao = new UsuarioDao();
			inicializaInterfaces();
		}
		return dao;
	}
	private static void inicializaInterfaces() {
		enderecoDao = EnderecoDao.getInstance();
		pessoaDAO = PessoaDao.getInstance();
	}
	
	private UsuarioDao(){
		initConnection();
	}
	
	
	
	
	@Override
	public Result salvar(Usuario usuario) {
		
		Result result = new Result();
		result.setMensagem("");
		result.setSucesso(false);
		Pessoa pessoaId = pessoaDAO.pesquisarCpfCnpj(usuario.getId_pessoa_cpf_cnpj());
		
		if(pessoaId == null){ // n�o pode inserir usuario com cpf iqual
		 	Result r = pessoaDAO.salvar(usuario.getPessoa());
			if(r.isSucesso()){
				
				pessoaId = pessoaDAO.pesquisarCpfCnpj(usuario.getId_pessoa_cpf_cnpj());
				usuario.setId_pessoa_cpf_cnpj(pessoaId.getId_pessoa_cpf_cnpj());
				
				String sql = "INSERT INTO usuario"
						+ "(login,"
						+ "senha,"
						+ "administrador,"
						+ "ativo,"
						+ "cpf_cnpj_pessoa) "
						+ "VALUES (?,?,?,?,?)";
				try {
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.setString(1, usuario.getLogin());
					ps.setString(2, usuario.getSenha());
					ps.setBoolean(3, usuario.getAdministrador());
					ps.setBoolean(4, usuario.getAtivo());
					ps.setString(5, usuario.getId_pessoa_cpf_cnpj());
					ps.execute();
					
					if(usuario.getEndereco()!=null){
						enderecoDao.salvar(usuario.getEndereco());
						if(enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj()) != null){
							usuario.getEndereco().setId(enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj()).getId());
						}
					}
					
					result.setMensagem("Usu�rio inserido com sucesso");
					result.setSucesso(true);
					
					return result;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}else{
			result.setMensagem("CPF/CNPJ repetido!");
			result.setSucesso(false);
		}
		return result;
	}

	@Override
	public Result excluir(Usuario usuario) {
		
		Result result = new Result();
		result.setSucesso(false);
		
		 String sql= "Delete from usuario where id_usuario = "+usuario.getId();
		 try {
				Statement st = conn.createStatement();
				st.executeUpdate(sql);
				result.setMensagem("Usu�rio excluido com sucesso");
				result.setSucesso(true);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 if(result.isSucesso()){
			 pessoaDAO.excluir(usuario.getPessoa());
		 }
		 
		return result;
	}

	@Override
	public Result alterar(Usuario usuario) {
		Boolean cpfCnpjOk  = true;
		Result result = new Result();
		result.setSucesso(false);
		
		 String sql= "UPDATE usuario SET login ='"+usuario.getLogin()+"', cpf_cnpj_pessoa ='"+usuario.getId_pessoa_cpf_cnpj()+"',senha = '"+usuario.getSenha()+"',administrador ="+usuario.getAdministrador()+","
		 			+"ativo = "+usuario.getAtivo()+" WHERE id_usuario = "+usuario.getId();
		 
			if(!usuario.getId_pessoa_cpf_cnpj().equals(usuario.getCpf_cnpj_aux_alterar())){
				if(!pessoaDAO.unicoCnpjExistente(usuario.getId_pessoa_cpf_cnpj())){
					cpfCnpjOk = false;
				}
			}
			
		if(cpfCnpjOk){
			 try {
					Statement st = conn.createStatement();
					st.executeUpdate(sql);
					
					result.setMensagem("Usu�rio alterado com sucesso");
					result.setSucesso(true);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 if(result.isSucesso()){
				 pessoaDAO.alterar(usuario.getPessoa());
			 }
		 }else{
			result.setMensagem("Usu�rio j� cadastrado com este CPF");
			result.setSucesso(false);
		 }
		return result;
	}

	@Override
	public List<Usuario> listObjects() {
		 List<Usuario> listUsuarios = new ArrayList<Usuario>();
		 String sql= "SELECT *  FROM usuario ";
		 
		 try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()){
				
				Usuario usuario = new Usuario();
				usuario.setId(rs.getInt("id_usuario"));
				usuario.setLogin(rs.getString("login"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setAdministrador(rs.getBoolean("administrador"));
				usuario.setAtivo(rs.getBoolean("ativo"));
				usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(usuario.getId_pessoa_cpf_cnpj());
				usuario.setNome(pessoa.getNome());
				usuario.setTelefone_ddd(pessoa.getTelefone_ddd());
				usuario.setTelefone_numero(pessoa.getTelefone_numero());
				usuario.setEmail(pessoa.getEmail());
				usuario.setInsc_estadual(pessoa.getInsc_estadual());
				
				Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj());
				usuario.setEndereco(endereco);
				
				listUsuarios.add(usuario);
			}
			
		} catch (Exception e) {
			System.out.println("Erro na listagem geral de usuarios");
			e.printStackTrace();
		}
		return listUsuarios;
	}

	@Override
	public Usuario pesquisarPorCpf(String cpfCnpjIdUsuario) {
		 String sql= "SELECT u.id_usuario, "
		 			+ "u.login, "
		 			+ "u.senha, "
		 			+ "u.administrador, "
		 			+ "u.ativo, "
		 			+ "u.cpf_cnpj_pessoa, "
		 			+ "p.nome, "
		 			+ "p.email, "
		 			+ "p.insc_estadual, "
		 			+ "p.telefone_ddd, "
		 			+ "p.telefone_numero "
		 			+ "FROM usuario u "
		 			+ "JOIN pessoa p on p.cpf_cnpj_pessoa = u.cpf_cnpj_pessoa "
		 			+ "WHERE u.cpf_cnpj_pessoa = ? OR u.id_usuario = ?";
		 
		 try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, cpfCnpjIdUsuario);
				
				try {
					ps.setInt(2, Integer.parseInt(cpfCnpjIdUsuario));
				} catch (Exception e) {
					ps.setNull(2, Types.INTEGER);
				}
				
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					
					Usuario usuario = new Usuario();
					usuario.setId(rs.getInt("id_usuario"));
					usuario.setLogin(rs.getString("login"));
					usuario.setSenha(rs.getString("senha"));
					usuario.setAdministrador(rs.getBoolean("administrador"));
					usuario.setAtivo(rs.getBoolean("ativo"));
					usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
					usuario.setNome(rs.getString("nome"));
					usuario.setTelefone_ddd(rs.getString("telefone_ddd"));
					usuario.setTelefone_numero(rs.getString("telefone_numero"));
					usuario.setEmail(rs.getString("email"));
					usuario.setInsc_estadual(rs.getString("insc_estadual"));
					
					Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj());
					usuario.setEndereco(endereco);
					
					return usuario;
				}
				
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de usuarios");
				e.printStackTrace();
			}
			return null;
	}
	
	@Override
	public List<Usuario> filtrar(String nome) {
		 String sql= "SELECT p.*, u.* FROM pessoa p JOIN usuario u ON u.cpf_cnpj_pessoa = p.cpf_cnpj_pessoa WHERE p.nome LIKE '%"+nome+"%' ";
		 List<Usuario> listUsuario = new ArrayList<>();
		 try {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(sql);
				while(rs.next()){
					Usuario usuario = new Usuario();
					usuario.setId(rs.getInt("id_usuario"));
					usuario.setLogin(rs.getString("login"));
					usuario.setSenha(rs.getString("senha"));
					usuario.setAdministrador(rs.getBoolean("administrador"));
					usuario.setAtivo(rs.getBoolean("ativo"));
					usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
					
					Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(usuario.getId_pessoa_cpf_cnpj());
					usuario.setNome(pessoa.getNome());
					usuario.setTelefone_ddd(pessoa.getTelefone_ddd());
					usuario.setTelefone_numero(pessoa.getTelefone_numero());
					usuario.setEmail(pessoa.getEmail());
					usuario.setInsc_estadual(pessoa.getInsc_estadual());
					
					Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj());
					usuario.setEndereco(endereco);
					
					listUsuario.add(usuario);
					
				}
				
				return listUsuario;
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de usuarios");
				e.printStackTrace();
			}
		 
			return null;
	}

	public Usuario pesquisarUsuarioPorId(Usuario usuarioAux){
		String sql= "SELECT *  FROM usuario WHERE id_usuario = "+usuarioAux.getId();
		 try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				Usuario usuario = new Usuario();
				usuario.setId(rs.getInt("id_usuario"));
				usuario.setLogin(rs.getString("login"));
				usuario.setSenha(rs.getString("senha"));
				usuario.setAdministrador(rs.getBoolean("administrador"));
				usuario.setAtivo(rs.getBoolean("ativo"));
				usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(usuario.getId_pessoa_cpf_cnpj());
				usuario.setNome(pessoa.getNome());
				usuario.setTelefone_ddd(pessoa.getTelefone_ddd());
				usuario.setTelefone_numero(pessoa.getTelefone_numero());
				usuario.setEmail(pessoa.getEmail());
				usuario.setInsc_estadual(pessoa.getInsc_estadual());
				
				Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(usuario.getId_pessoa_cpf_cnpj());
				usuario.setEndereco(endereco);
				
				return usuario;
			}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de usuarios");
				e.printStackTrace();
			}
		 
			return null;
	}
	
	@Override
	public Boolean exist(Usuario usuario) {
		String sql= "SELECT *  FROM usuario WHERE id_usuario = "+usuario.getId();
		 try {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(sql);
				
				if(rs.isBeforeFirst()){
					return true;
				}else{
					return false;
				}
				
			} catch (Exception e) {
				System.out.println("Erro no exist de usuarios");
				e.printStackTrace();
			}
		 
		return false;
	}
	
	public Usuario pesquisarUsuario(String login) {
		 String sql= "SELECT u.id_usuario, "
		 			+ "u.login, "
		 			+ "u.senha, "
		 			+ "u.administrador, "
		 			+ "u.cpf_cnpj_pessoa,"
		 			+ "u.ativo "
		 			+ "FROM usuario u "
		 			+ "WHERE u.login = "
		 			+ "'"
		 			+ login
		 			+ "'";
		 try {
				PreparedStatement ps = conn.prepareStatement(sql);
				
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					
					Usuario usuario = new Usuario();
					usuario.setId(rs.getInt("id_usuario"));
					usuario.setLogin(rs.getString("login"));
					usuario.setSenha(rs.getString("senha"));
					usuario.setAdministrador(rs.getBoolean("administrador"));
					usuario.setCpf_cnpj_pessoa(rs.getString("cpf_cnpj_pessoa"));
					usuario.setAtivo(rs.getBoolean("ativo"));
					
					return usuario;
				}
				
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de usuarios");
				e.printStackTrace();
			}
			return null;
	}
}
