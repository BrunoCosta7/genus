package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Fornecedor;
import model.Produto;
import utils.Result;
import InterfacesDao.IProdutoDao;

public class ProdutoDao extends ComunDao implements IProdutoDao{

	
	private static ProdutoDao dao = null;
	public static IProdutoDao produtoDao;
	
	public static ProdutoDao getInstance(){
		if (dao == null){
			dao = new ProdutoDao();
			inicializaInterfaces();
		}
		return dao;
	}
	
	private static void inicializaInterfaces(){
		produtoDao = ProdutoDao.getInstance();
	}
	
	private ProdutoDao(){
		initConnection();
	}
	
	@Override
	public Result salvar(Produto produto) {
		
		Result result = new Result();
		result.setMensagem("");
		result.setSucesso(false);
		
		String sql = "INSERT INTO produto"
				+"(nome,"
				+"medida,"
				+"quantidade,"
				+"id_fornecedor,"
				+"preco_custo,"
				+"preco_venda) "
				+"VALUES (?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, produto.getNome());
			ps.setString(2, produto.getMedida());
			ps.setInt(3, produto.getQuantidade());
			ps.setInt(4, produto.getFornecedor().getId());
			ps.setDouble(5, produto.getPrecoCusto());
			ps.setDouble(6, produto.getPrecoVenda());
			ps.execute();
			
			sql = "SELECT MAX(id_produto) as id_produto FROM produto";
			ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				produto.setId(rs.getInt("id_produto"));
			}
			
			
			result.setMensagem("Produto inserido com sucesso!");
			result.setSucesso(true);
			
			return result;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Result excluir(Produto produto) {
		 Result result = new Result();
		 result.setSucesso(false);
		 
		 String sql = "UPDATE produto SET ativo = false WHERE id_produto = " + produto.getId();
		 try {
			Statement st = conn.createStatement();
			st.executeUpdate(sql);
			result.setMensagem("Produto exclu�do com sucesso!");
			result.setSucesso(true);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		 
		return result;
	}

	@Override
	public Result alterar(Produto produto) {
		Result result = new Result();
		result.setSucesso(false);
		
		String sql = "UPDATE produto SET nome ='" + produto.getNome()+"', medida ='" + produto.getMedida()+"', quantidade ='" 
		+ produto.getQuantidade()+"', id_fornecedor ="
		+ produto.getFornecedor().getId()+", preco_venda =" + produto.getPrecoVenda()+", preco_custo =" + produto.getPrecoCusto()+
		" WHERE id_produto = "+produto.getId();
		try {
			Statement st = conn.createStatement();
			st.executeUpdate(sql);
			
			result.setMensagem("Produto alterado com sucesso!");
			result.setSucesso(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public List<Produto> listObjects() {
		List<Produto> listProdutos = new ArrayList<Produto>();
		
		//Fazer JOIN com o fornecedor e criar um objeto fornecedor e setar no produto
		String sql = "SELECT "
				+ "p.nome as nomeProduto,"
				+ "p.id_produto,"
				+ "p.medida,"
				+ "p.quantidade,"
				+ "p.preco_custo,"
				+ "p.preco_venda,"
				+ "f.id_fornecedor,"
				+ "f.nome_fantasia,"
				+ "ps.nome nomeFornecedor "
				+ "FROM produto p "
				+ "INNER JOIN fornecedor f ON f.id_fornecedor = p.id_fornecedor "
				+ "INNER JOIN pessoa ps ON ps.cpf_cnpj_pessoa = f.cpf_cnpj_pessoa WHERE p.ativo = true";
		
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while(rs.next()){
				
				Produto produto = new Produto();
				produto.setId(rs.getInt("id_produto"));
				produto.setNome(rs.getString("nomeProduto"));
				produto.setMedida(rs.getString("medida"));
				produto.setQuantidade(rs.getInt("quantidade"));
				produto.setPrecoCusto(rs.getDouble("preco_custo"));
				produto.setPrecoVenda(rs.getDouble("preco_venda"));
				
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId_fornecedor(rs.getInt("id_fornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setNome(rs.getString("nomeFornecedor"));
				
				produto.setFornecedor(fornecedor);
				
				listProdutos.add(produto);
			}
			return listProdutos;
			
		} catch (Exception e) {
			System.out.println("Erro ao listar produtos");
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Produto pesquisarPorNomeMedida(String nome_medida_produto) {
		
		//Fazer JOIN com o fornecedor e criar um objeto fornecedor e setar no produto
		String sql = "SELECT p.nome, "
				+"p.medida, "
				+"p.id_produto, "
				+"p.quantidade, "
				+"p.id_fornecedor, "
				+"p.preco_custo, "
				+"p.preco_venda, "
				+"f.id_fornecedor, "
				+"f.nome_fantasia, "
				+"f.cpf_cnpj_pessoa, "
				+"ps.nome nomeFornecedor "
				+"FROM produto p "
				+"LEFT JOIN fornecedor f ON f.id_fornecedor = p.id_fornecedor "
				+"INNER JOIN pessoa ps ON ps.cpf_cnpj_pessoa = f.cpf_cnpj_pessoa "
				+"WHERE p.nome = ? OR p.medida = ? and p.ativo = true";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nome_medida_produto);
			try {
				ps.setString(2, nome_medida_produto);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Erro na pesquisa de produtos por nome!");
			}
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
			
			Produto produto = new Produto();
			produto.setNome(rs.getString("nome"));
			produto.setMedida(rs.getString("medida"));
			produto.setId(rs.getInt("id_produto"));
			produto.setQuantidade(rs.getInt("quantidade"));
			produto.setPrecoCusto(rs.getDouble("preco_custo"));
			produto.setPrecoVenda(rs.getDouble("preco_venda"));
			
			Fornecedor fornecedor = new Fornecedor();
			fornecedor.setId_fornecedor(rs.getInt("id_fornecedor"));
			fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
			fornecedor.setNome(rs.getString("nome"));
			
			produto.setFornecedor(fornecedor);
			
			return produto;
			}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Erro na pesquisa de produtos por medida!");
		}
		
//		try {
//			PreparedStatement ps = conn.prepareStatement(sql);
//			
//			ResultSet rs = ps.executeQuery();
//			
//			while (rs.next());
//			Produto produto = new Produto();
//			produto.setId(rs.getInt("id"));
//			produto.setNome(rs.getString("1, nome"));
//			produto.setMedida(rs.getString("2, medida"));
//			produto.setQuantidade(rs.getInt("quantidade"));
////			produto.setFornecedor(rs.getString("fornecedor"));
//			produto.setPrecoCusto(rs.getDouble("preco_custo"));
//			produto.setPrecoVenda(rs.getDouble("preco_venda"));
//			
//			Fornecedor fornecedor = new Fornecedor();
//			fornecedor.setId_fornecedor(rs.getInt("id_fornecedor"));
//			fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
//			fornecedor.setNome(rs.getString("nome"));
//			
//			produto.setFornecedor(fornecedor);
//			
//			return produto;
//			
//		} catch (Exception e) {
//			System.out.println("Erro na pesquisa de produtos");
//			e.printStackTrace();
//		}
//				
		return null;
	}

	@Override
	public List<Produto> filtrar(String nome, String medida) {
		int aux = 0;
		int indiceNome = 0;
		int indiceMedida = 0;
		
		String nomeAux = "%"+nome+"%";
		String medidaAux = "%"+medida+"%";
		
		String sql = "SELECT p.*, f.*, ps.nome nomeFornecedor "
				+ "FROM produto p "
				+ "LEFT JOIN fornecedor f ON p.id_fornecedor = f.id_fornecedor "
				+ "INNER JOIN pessoa ps ON ps.cpf_cnpj_pessoa = f.cpf_cnpj_pessoa "
				+ "WHERE 1=1";
		
		if(nome!=null && !nome.equals("")){
			sql+="AND p.nome LIKE ? ";
			aux++;
			indiceNome = aux;
		}
		
		if(medida != null && !medida.equals("")){
			sql+=" AND p.medida ?";
			aux++;
			indiceMedida = aux;
		}
		
		sql+="AND p.ativo = true";
		
		List <Produto> listProduto = new ArrayList<>();
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			
			if(nome!=null && !nome.equals("")){
				ps.setString(indiceNome, nomeAux);
			}
			
			if(medida != null && !medida.equals("")){
				ps.setString(indiceMedida, medidaAux);
			}
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				Produto produto = new Produto();
				produto.setId(rs.getInt("id_produto"));
				produto.setNome(rs.getString("nome"));
				produto.setMedida(rs.getString("medida"));
				produto.setQuantidade(rs.getInt("quantidade"));
				produto.setPrecoCusto(rs.getDouble("preco_custo"));
				produto.setPrecoVenda(rs.getDouble("preco_venda"));
				
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId_fornecedor(rs.getInt("id_fornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setNome(rs.getString("nomeFornecedor"));
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				produto.setFornecedor(fornecedor);
				
				listProduto.add(produto);
			}
			
			return listProduto;
			
		} catch (Exception e) {
			System.out.println("Erro na pesquisa de produtos");
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public boolean exist(Produto produto) {
		String sql = "SELECT id FROM usuario WHERE id = "+ produto;
		
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			if (rs.getFetchSize() > 0){
				return true;
			} else{
				return false;
			}
		} catch (Exception e) {
			System.out.println("Erro no exist de produtos");
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public Produto pesquisarPorId(Integer id_produto) {
		
		String sql = "SELECT "
				+ "p.id_produto, "
				+ "p.nome, "
				+ "p.medida, "
				+ "p.quantidade, "
				+ "p.preco_custo, "
				+ "p.preco_venda, "
				+ "f.id_fornecedor,"
				+ "f.nome_fantasia,"
				+ "f.cpf_cnpj_pessoa,"
				+ "ps.nome nomeFornecedor "
				+ " FROM produto p "
				+ "LEFT JOIN fornecedor f ON f.id_fornecedor = p.id_fornecedor "
				+ "INNER JOIN pessoa ps ON ps.cpf_cnpj_pessoa = f.cpf_cnpj_pessoa "
				+ "WHERE p.id_produto = ? and p.ativo = true";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id_produto);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				Produto produto = new Produto();
				
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setNome(rs.getString("nomeFornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setId(rs.getInt("id_fornecedor"));
				fornecedor.setId_fornecedor(rs.getInt("id_fornecedor"));
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				produto.setFornecedor(fornecedor);
				
				produto.setNome(rs.getString("nome"));
				produto.setMedida(rs.getString("medida"));
				produto.setQuantidade(rs.getInt("quantidade"));
				produto.setId(rs.getInt("id_produto"));
				produto.setPrecoCusto(rs.getDouble("preco_custo"));
				produto.setPrecoVenda(rs.getDouble("preco_venda"));
				
				return produto;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

