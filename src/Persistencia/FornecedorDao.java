package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import InterfacesDao.IEnderecoDao;
import InterfacesDao.IFornecedorDao;
import InterfacesDao.IPessoaDao;
import model.Endereco;
import model.Fornecedor;
import model.Pessoa;
import utils.Result;

public class FornecedorDao extends ComunDao implements IFornecedorDao{
	
	private static IEnderecoDao enderecoDao;
	private static IPessoaDao pessoaDAO;
	
	private static FornecedorDao dao = null;
	
	public static FornecedorDao getInstance(){
		if(dao==null){
			dao = new FornecedorDao();
			inicializaInterfaces();
		}
		return dao;
	}
	private static void inicializaInterfaces() {
		enderecoDao = EnderecoDao.getInstance();
		pessoaDAO = PessoaDao.getInstance();
	}
	
	private FornecedorDao(){
		initConnection();
	}
	
	
	@Override
	public Fornecedor pesquisarPorNomeFantasia(String nome_fantasia) {
		String sql= "SELECT u.id_fornecedor, "
	 			+ "u.nome_fantasia, "
	 			+ "u.ativo, "
	 			+ "u.cpf_cnpj_pessoa, "
	 			+ "p.nome, "
	 			+ "p.email, "
	 			+ "p.insc_estadual, "
	 			+ "p.telefone_ddd, "
	 			+ "p.telefone_numero "
	 			+ "FROM fornecedor u "
	 			+ "JOIN pessoa p on p.cpf_cnpj_pessoa = u.cpf_cnpj_pessoa "
	 			+ "WHERE u.nome_fantasia = ?";
	 
	 try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nome_fantasia);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId(rs.getInt("id_Fornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setAtivo(rs.getBoolean("ativo"));
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				fornecedor.setNome(rs.getString("nome"));
				fornecedor.setTelefone_ddd(rs.getString("telefone_ddd"));
				fornecedor.setTelefone_numero(rs.getString("telefone_numero"));
				fornecedor.setEmail(rs.getString("email"));
				fornecedor.setInsc_estadual(rs.getString("insc_estadual"));
				
				Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj());
				fornecedor.setEndereco(endereco);
				
				return fornecedor;
			}
	 
	 	} catch (Exception e) {
			System.out.println("Erro na pesquisa de fornecedores");
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public Result salvar(Fornecedor fornecedor) {
		
		Result result = new Result();
		result.setMensagem("");
		result.setSucesso(false);
		
		Pessoa pessoaId = pessoaDAO.pesquisarCpfCnpj(fornecedor.getId_pessoa_cpf_cnpj());
		
		if(pessoaId == null){ // n�o pode inserir fornecedor com cpf iqual
		 	Result r = pessoaDAO.salvar(fornecedor.getPessoa());
		 	
		 	if(r.isSucesso()){
		 		pessoaId = pessoaDAO.pesquisarCpfCnpj(fornecedor.getId_pessoa_cpf_cnpj());
		 		fornecedor.setId_pessoa_cpf_cnpj(pessoaId.getId_pessoa_cpf_cnpj());
		 		String sql = "INSERT INTO fornecedor"
		 				+ "(nome_fantasia,"
		 				+ "ativo,"
		 				+ "cpf_cnpj_pessoa)"
		 				+ "VALUES (?,?,?)";
		 		try {
					PreparedStatement ps = conn.prepareStatement(sql);
					ps.setString(1, fornecedor.getNome_fantasia());
					ps.setBoolean(2, fornecedor.getAtivo());
					ps.setString(3, fornecedor.getId_pessoa_cpf_cnpj());
					ps.execute();
					
					result.setMensagem("Fornecedor inserido com sucesso");
					result.setSucesso(true);
					
					if(fornecedor.getEndereco()!=null){
						enderecoDao.salvar(fornecedor.getEndereco());
						if(enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj()) != null){
							fornecedor.getEndereco().setId(enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj()).getId());
						}
					}
					
					return result;
				} catch (SQLException e) {
					e.printStackTrace();
				}
		 	}
		}else{
			result.setMensagem("CPF/CNPJ repetido!");
			result.setSucesso(false);
		}
	 	
		
		
		return result;
	}

	@Override
	public Result excluir(Fornecedor fornecedor) {
		
		Result result = new Result();
		result.setSucesso(false);
		
//		 String sql= "Delete from fornecedor where id_fornecedor = "+fornecedor.getId();
		 String sql= "UPDATE fornecedor SET ativo = "+false+" where id_Fornecedor ="+fornecedor.getId(); // regra de caso de uso, n�o excluir fornecedor, apenas mudar pra n�o ativo
		 try {
				Statement st = conn.createStatement();
				st.executeUpdate(sql);
				result.setMensagem("Fornecedor desativado com sucesso");
				result.setSucesso(true);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
//		 if(result.isSucesso()){
//			 pessoaDAO.excluir(fornecedor.getPessoa());
//		 }
		 
		return result;
	}

	@Override
	public Result alterar(Fornecedor fornecedor) {
		Boolean cpfCnpjOk  = true;
		Result result = new Result();
		result.setSucesso(false);
		String sql= "UPDATE fornecedor SET nome_fantasia = '"+fornecedor.getNome_fantasia()+"', cpf_cnpj_pessoa ='"+fornecedor.getId_pessoa_cpf_cnpj()+"',ativo = "+fornecedor.getAtivo()+" where id_Fornecedor ="+fornecedor.getId();
		
		if(!fornecedor.getId_pessoa_cpf_cnpj().equals(fornecedor.getCpf_cnpj_aux_alterar())){
			if(!pessoaDAO.unicoCnpjExistente(fornecedor.getId_pessoa_cpf_cnpj())){
				cpfCnpjOk = false;
			}
		}
		 if(cpfCnpjOk){
			 try {
					Statement st = conn.createStatement();
					st.executeUpdate(sql);
					
					result.setMensagem("Fornecedor alterado com sucesso");
					result.setSucesso(true);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 if(result.isSucesso()){
				 pessoaDAO.alterar(fornecedor.getPessoa());
			 }
		 }else{
			result.setMensagem("Fornecedor j� cadastrado com este CPF");
			result.setSucesso(false);
		 }
	
		return result;
	}

	@Override
	public List<Fornecedor> listObjects() {
		 List<Fornecedor> listfornecedors = new ArrayList<Fornecedor>();
//		 String sql= "SELECT *  FROM fornecedor where ativo = "+ true;
		 String sql= "SELECT *  FROM fornecedor" ;
		 
		 try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId(rs.getInt("id_Fornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setAtivo(rs.getBoolean("ativo"));
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(fornecedor.getId_pessoa_cpf_cnpj());
				fornecedor.setNome(pessoa.getNome());
				fornecedor.setTelefone_ddd(pessoa.getTelefone_ddd());
				fornecedor.setTelefone_numero(pessoa.getTelefone_numero());
				fornecedor.setEmail(pessoa.getEmail());
				fornecedor.setInsc_estadual(pessoa.getInsc_estadual());
				
				Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj());
				fornecedor.setEndereco(endereco);
				
				listfornecedors.add(fornecedor);
			}
			
		} catch (Exception e) {
			System.out.println("Erro na listagem geral de fornecedores");
			e.printStackTrace();
		}
		return listfornecedors;
	}

	@Override
	public Fornecedor pesquisarPorCpf(String cpfCnpjIdfornecedor) {
		 String sql= "SELECT u.id_fornecedor, "
		 			+ "u.nome_fantasia, "
		 			+ "u.ativo, "
		 			+ "u.cpf_cnpj_pessoa, "
		 			+ "p.nome, "
		 			+ "p.email, "
		 			+ "p.insc_estadual, "
		 			+ "p.telefone_ddd, "
		 			+ "p.telefone_numero "
		 			+ "FROM fornecedor u "
		 			+ "JOIN pessoa p on p.cpf_cnpj_pessoa = u.cpf_cnpj_pessoa "
		 			+ "WHERE u.cpf_cnpj_pessoa = ? OR u.id_Fornecedor = ?";
		 
		 try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, cpfCnpjIdfornecedor);
				
				try {
					ps.setInt(2, Integer.parseInt(cpfCnpjIdfornecedor));
				} catch (Exception e) {
					ps.setNull(2, Types.INTEGER);
				}
				
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					Fornecedor fornecedor = new Fornecedor();
					fornecedor.setId(rs.getInt("id_Fornecedor"));
					fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
					fornecedor.setAtivo(rs.getBoolean("ativo"));
					fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
					
					fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
					fornecedor.setNome(rs.getString("nome"));
					fornecedor.setTelefone_ddd(rs.getString("telefone_ddd"));
					fornecedor.setTelefone_numero(rs.getString("telefone_numero"));
					fornecedor.setEmail(rs.getString("email"));
					fornecedor.setInsc_estadual(rs.getString("insc_estadual"));
					
					Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj());
					fornecedor.setEndereco(endereco);
					
					return fornecedor;
				}
		 
		 	} catch (Exception e) {
				System.out.println("Erro na pesquisa de fornecedores");
				e.printStackTrace();
			}
			return null;
	}
	
	@Override
	public List<Fornecedor> filtrar(String nome) {
		List<Fornecedor> listFornecedors = new ArrayList<Fornecedor>();
		String sql= "SELECT p.*, u.* FROM pessoa p JOIN fornecedor u ON u.cpf_cnpj_pessoa = p.cpf_cnpj_pessoa WHERE u.nome_fantasia LIKE '%"+nome+"%'";
		
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				Fornecedor fornecedor = new Fornecedor();
				fornecedor.setId(rs.getInt("id_Fornecedor"));
				fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
				fornecedor.setAtivo(rs.getBoolean("ativo"));
				fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				
				Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(fornecedor.getId_pessoa_cpf_cnpj());
				fornecedor.setNome(pessoa.getNome());
				fornecedor.setTelefone_ddd(pessoa.getTelefone_ddd());
				fornecedor.setTelefone_numero(pessoa.getTelefone_numero());
				fornecedor.setEmail(pessoa.getEmail());
				fornecedor.setInsc_estadual(pessoa.getInsc_estadual());
				
				Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj());
				fornecedor.setEndereco(endereco);
				
				listFornecedors.add(fornecedor);
			}
			return listFornecedors;
		} catch (Exception e) {
			System.out.println("Erro na pesquisa de usuarios");
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Fornecedor exist(Fornecedor fornecedorAux) {
		String sql= "SELECT *  FROM fornecedor WHERE id_fornecedor = "+fornecedorAux.getId();
		
		 try {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(sql);
				

				while(rs.next()){
					Fornecedor fornecedor = new Fornecedor();
					fornecedor.setId(rs.getInt("id_fornecedor"));
					fornecedor.setNome_fantasia(rs.getString("nome_fantasia"));
					fornecedor.setAtivo(rs.getBoolean("ativo"));
					fornecedor.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
					
					Pessoa pessoa = pessoaDAO.pesquisarCpfCnpj(fornecedor.getId_pessoa_cpf_cnpj());
					fornecedor.setNome(pessoa.getNome());
					fornecedor.setTelefone_ddd(pessoa.getTelefone_ddd());
					fornecedor.setTelefone_numero(pessoa.getTelefone_numero());
					fornecedor.setEmail(pessoa.getEmail());
					fornecedor.setInsc_estadual(pessoa.getInsc_estadual());
					
					Endereco endereco = enderecoDao.pesquisaEnderecoPorPessoa(fornecedor.getId_pessoa_cpf_cnpj());
					fornecedor.setEndereco(endereco);
					
					return fornecedor;
				}
			} catch (Exception e) {
				System.out.println("Erro no exist de fornecedores");
				e.printStackTrace();
			}
		 
		return null;
	}
}
