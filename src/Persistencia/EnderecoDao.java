package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Cidade;
import model.Endereco;
import model.Estado;
import utils.Result;
import InterfacesDao.IEnderecoDao;

public class EnderecoDao extends ComunDao implements IEnderecoDao {
	
	private static EnderecoDao dao = null;
	
	public static EnderecoDao getInstance(){
		if(dao==null){
			dao = new EnderecoDao();
		}
		return dao;
	}
	
	
	private EnderecoDao(){
		initConnection();
	}
	
	public Result salvar(Endereco endereco){
		
		Result result = new Result();
		result.setSucesso(false);
		
		if(endereco != null){
			getEstado(endereco.getCidade().getEstado());
			getCidade(endereco.getCidade());
			
			String sql = "insert into \"endereco\"( logradouro,"
						 + "numero,"
						 + "id_pessoa_cpf_cnpj,"
						 + "complemento,"
						 + "bairro,"
						 + "id_cidade)"
						 + "values (?,?,?,?,?,?)";
			try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, endereco.getLogradouro());
				ps.setInt(2, endereco.getNumero());
				ps.setString(3, endereco.getId_pessoa_cpf_cnpj());
				ps.setString(4, endereco.getComplemento());
				ps.setString(5, endereco.getBairro());
				ps.setInt(6, endereco.getCidade().getId_cidade());
				ps.execute();
				result.setSucesso(true);
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int pesquisaEnderecoPorUsuario(int id_usuario){
		String sql = "Select id from endereco where id_usuario = ?";
		
		try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					return rs.getInt("id");
				}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de enderecos");
				e.printStackTrace();
			}
		
		return 0;
	}
	
	
	public Endereco pesquisaEnderecoPorPessoa(String id_pessoa){
		String sql = "SELECT "
				+ " en.id, "
				+ " en.logradouro, "
				+ " en.numero, "
				+ " en.complemento, "
				+ " en.bairro, "
				+ " en.id_cidade, "
				+ " c.nome_cidade, "
				+ " c.id_cidade, "
				+ " es.id_estado, "
				+ " es.nome_estado "
				+ " FROM endereco en "
				+ "	JOIN cidade c on c.id_cidade = en.id_cidade "
				+ "	JOIN estado es on es.id_estado = c.id_estado "
				+ " WHERE en.id_pessoa_cpf_cnpj = ?";
		
		try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, id_pessoa);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					
					Endereco  endereco = new Endereco();
					endereco.setId(rs.getInt("id"));
					endereco.setLogradouro(rs.getString("logradouro"));
					endereco.setNumero(rs.getInt("numero"));
					endereco.setComplemento(rs.getString("complemento"));
					endereco.setBairro(rs.getString("bairro"));
					
					Estado  estado = new Estado();
					estado.setId_estado(rs.getString("id_estado"));
					estado.setNomeEstado(rs.getString("nome_estado"));
					
					Cidade cidade = new Cidade();
					cidade.setId_cidade(rs.getInt("id_cidade"));
					cidade.setNomeCidade(rs.getString("nome_cidade"));
					cidade.setEstado(estado);
					
					endereco.setCidade(cidade);
					
					
					return endereco;
					
				}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de enderecos");
				e.printStackTrace();
			}
		
		return null;
	}
	
	public Result verificarEstadoCidade(Cidade cidade){
		
		Result result = new Result();
		result.setMensagem("");
		result.setSucesso(false);
		
		
		if(getEstado(cidade.getEstado())){
			String sql = "Select id_estado from cidade where nome_cidade = ? ";
			
			try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, cidade.getNome_cidade().toLowerCase());
				ResultSet rs = ps.executeQuery();
				boolean achouCidade = false;
				while(rs.next()){
					if(cidade.getEstado().getId_estado().equals(rs.getString("id_estado"))){
						result.setSucesso(true);
						achouCidade = true;
						break;
					}
				}
				if(!achouCidade){
					result.setSucesso(false);
					result.setMensagem("Cidade n�o encontrada para o estado informado");
				}
				
			} catch (Exception e) {
				System.out.println("Erro na verifica��o de estados e cidades");
				e.printStackTrace();
			}
			
		}else{
			result.setMensagem("Estado n�o encontrado");
			result.setSucesso(false);
		}
		
		
		return result;
	}
	public Boolean getEstado(Estado estado){
		String sql = "Select id_estado from estado where nome_estado = ?";
		
		try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, estado.getNome_estado().toLowerCase());
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					estado.setId_estado(rs.getString("id_estado"));
					return true;
					
				}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de estados");
				e.printStackTrace();
			}
		return false;
	}
	
	public Estado getEstado(String id_estado){
		String sql = "Select * from estado where id_estado = ?";
		
		try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, id_estado);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()){
					Estado  estado = new Estado();
					estado.setId_estado(rs.getString("id_estado"));
					estado.setNomeEstado(rs.getString("nome_estado"));
					return estado;
				}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de estados");
				e.printStackTrace();
			}
		return null;
	}
	
	
	
	public Boolean getCidade(Cidade cidade){
		String sql = "Select id_cidade from cidade where nome_cidade = ? and id_estado = ?";
		
		try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, cidade.getNome_cidade().toLowerCase());
				ps.setString(2, cidade.getEstado().getId_estado());
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					cidade.setId_cidade(rs.getInt("id_cidade"));
					return true;
					
				}
			} catch (Exception e) {
				System.out.println("Erro na pesquisa de cidades");
				e.printStackTrace();
			}
		return false;
	}
	
	public List<Cidade> getCidades(String idEstado){
		
		List<Cidade> listaCidades = new ArrayList<Cidade>();
		String sql = "Select * from cidade where id_estado = ?";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, idEstado);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				Cidade cidade = new Cidade();
				cidade.setId_cidade(rs.getInt("id_cidade"));
				cidade.setNomeCidade(rs.getString("nome_cidade"));
				cidade.setEstado(getEstado(rs.getString("id_estado")));
				listaCidades.add(cidade);
			}
		} catch (Exception e) {
			System.out.println("Erro na pesquisa de cidades");
			e.printStackTrace();
		}
		
		return listaCidades;
	}

	public List<Estado> getEstados(){
		
		List<Estado> listaEstados = new ArrayList<Estado>();
		String sql = "Select * from estado";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				Estado estado = new Estado();
				estado.setId_estado(rs.getString("id_estado"));
				estado.setNomeEstado(rs.getString("nome_estado"));
				listaEstados.add(estado);
			}
		} catch (Exception e) {
			System.out.println("Erro ao pesquisar estados");
			e.printStackTrace();
		}
		
		return listaEstados;
	}

	@Override
	public Result alterar(Endereco endereco) {
		
		Result result = new Result();
		result.setSucesso(false);
		
		 String sql= "Update endereco SET logradouro ='"+endereco.getLogradouro()+"', numero = "+endereco.getNumero()+",id_pessoa_cpf_cnpj = '"+endereco.getId_pessoa_cpf_cnpj()+"', complemento = '"+endereco.getComplemento()+"', bairro = '"+endereco.getBairro()+"' where id = "+endereco.getId();
		 
		 try {
				Statement st = conn.createStatement();
				st.executeUpdate(sql);
				result.setSucesso(true);
				result.setMensagem("Endereco alterado com sucesso");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		return result;
	}


	@Override
	public Result excluir(Endereco endereco) {
		
		Result result = new Result();
		result.setSucesso(false);
		
		String sql= "Delete from endereco where id = "+endereco.getId();
		 
		 try {
				Statement st = conn.createStatement();
				st.executeUpdate(sql);
				result.setSucesso(true);
				result.setMensagem("Endereco excluido com sucesso");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		return result;
	}


}
