package Persistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import model.Endereco;
import model.Pessoa;
import utils.Result;
import InterfacesDao.IEnderecoDao;
import InterfacesDao.IPessoaDao;

public class PessoaDao extends ComunDao implements IPessoaDao {
	
	private static PessoaDao dao = null;
	private static IEnderecoDao iEnderecoDao;
	
	public static PessoaDao getInstance(){
		if(dao==null){
			dao = new PessoaDao();
			inicializaInterfaces();
		}
		return dao;
	}
	private static void inicializaInterfaces() {
		iEnderecoDao = EnderecoDao.getInstance();
	}
	private PessoaDao(){
		initConnection();
	}
	
	
	@Override
	public Result salvar(Pessoa pessoa) {
		
		Result result = new Result("",false);
		
		String sql = "INSERT INTO pessoa "
					+"(cpf_cnpj_pessoa, "
					+ "nome, "
					+ "telefone_ddd, "
					+ "telefone_numero, "
					+ "email, "
					+ "insc_estadual, "
					+ "id_endereco ) VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, pessoa.getId_pessoa_cpf_cnpj());
			ps.setString(2, pessoa.getNome());
			ps.setString(3, pessoa.getTelefone_ddd());
			ps.setString(4, pessoa.getTelefone_numero());
			ps.setString(5, pessoa.getEmail());
			ps.setString(6, pessoa.getInsc_estadual());
			
			if(pessoa.getEndereco()!=null && pessoa.getEndereco().getId()>0){
				ps.setInt(7, pessoa.getEndereco().getId());
			}else{
				ps.setNull(7, Types.INTEGER);
			}
			
			ps.execute();
			
			result.setMensagem("Usu�rio salvo com sucesso");
			result.setSucesso(true);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public Boolean unicoCnpjExistente(String cpf_cnpj) {
		String sql = "select * from pessoa where cpf_cnpj_pessoa ='"+cpf_cnpj+"'";
		try {
			Statement st = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				if(rs.getString(("cpf_cnpj_pessoa")).equalsIgnoreCase(cpf_cnpj)){
					return false;
				}
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return  true;
	}
	
	
	@Override
	public Pessoa pesquisarCpfCnpj(String cpf_cnpj) {
		String sql = "select * from pessoa where cpf_cnpj_pessoa ='"+cpf_cnpj+"'";
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()){
				Pessoa pessoa = new Pessoa();
				pessoa.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				pessoa.setNome(rs.getString("nome"));
				pessoa.setEmail(rs.getString("email"));
				pessoa.setInsc_estadual(rs.getString("insc_estadual"));
				pessoa.setTelefone_ddd(rs.getString("telefone_ddd"));
				pessoa.setTelefone_numero(rs.getString("telefone_numero"));
				
				return pessoa;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public Boolean alterar(Pessoa pessoa) {
		 String sql= "Update pessoa SET nome ='"+pessoa.getNome()+"', telefone_ddd = '"+pessoa.getTelefone_ddd()+"', telefone_numero = '"+pessoa.getTelefone_numero()+"', cpf_cnpj_pessoa ='"+pessoa.getId_pessoa_cpf_cnpj()+"', email = '"+pessoa.getEmail()+"',"+
		 		"insc_estadual = '"+pessoa.getInsc_estadual()+"' where cpf_cnpj_pessoa = '"+pessoa.getCpf_cnpj_aux_alterar()+"'"; 
		 Boolean pessoaAlterado = false;
		 try {
				Statement st = conn.createStatement();
				st.executeUpdate(sql);
				pessoaAlterado = true;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 if(pessoaAlterado){
			 pessoa.getEndereco().setId_pessoa_cpf_cnpj(pessoa.getId_pessoa_cpf_cnpj());
			 Endereco enderecoAux = iEnderecoDao.pesquisaEnderecoPorPessoa(pessoa.getCpf_cnpj_aux_alterar());
			 pessoa.getEndereco().setId(enderecoAux.getId());
			 iEnderecoDao.alterar(pessoa.getEndereco());
		 }
		return null;
	}

	@Override
	public Result excluir(Pessoa pessoa) {
		Result result = new Result("", false); 
		String sql= "DELETE FROM pessoa WHERE cpf_cnpj_pessoa = ?";
		
		 try {
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, pessoa.getId_pessoa_cpf_cnpj());
				ps.executeUpdate();
				
				result.setSucesso(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		 
		 if(result.isSucesso() && pessoa.getEndereco() != null){
			 iEnderecoDao.excluir(pessoa.getEndereco());
		 }
		 
		return result;
	}

	
}
