package Persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import InterfacesDao.IComunDao;

public abstract class ComunDao implements IComunDao{

	public static Connection conn = null;
	
	public void initConnection() {
		try {
			String driver = "org.postgresql.Driver";
			Class.forName(driver);
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5433/genus","postgres","");
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Erro de driver do postgres");
			e.printStackTrace();
		}
	}

}
