package Persistencia;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.Historico;
import model.Produto;
import model.TipoAlteracao;
import model.Usuario;
import utils.Result;
import utils.UtilProject;
import InterfacesDao.IHistoricoDao;

public class HistoricoDao extends ComunDao implements IHistoricoDao{

	private static HistoricoDao dao = null;
	
	public static HistoricoDao getInstance(){
		if(dao == null){
			dao = new HistoricoDao();
		}
		return dao;
	}
	
	public HistoricoDao() {
		initConnection();
	}
	
	@Override
	public Result salvar(Historico historico) {
		Result result = new Result("", false);
	
		String sql = "INSERT INTO historico( "
					+ "id_produto, "
					+ "id_usuario, "
					+ "data_hora, "
					+ "valor_antigo, "
					+ "valor_novo, "
					+ "id_tipo_movimentacao) "
					+ "VALUES(?,?,?,?,?,?)";
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, historico.getProduto().getId());
			ps.setInt(2, historico.getUsuario().getId());
			ps.setDate(3, new Date(historico.getData_hora().getTime()));
			ps.setString(4, historico.getValor_antigo());
			ps.setString(5, historico.getValor_novo());
			ps.setInt(6, historico.getTipo_alteracao().getId_tipo_alteracao());
			ps.execute();
			result.setSucesso(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public List<Historico> listarTodos() {
		List<Historico> listaHistorico = new ArrayList<Historico>();
		
		String sql = "SELECT "
					+ "h.id_historico, "
					+ "h.data_hora, "
					+ "h.id_produto, "
					+ "h.id_usuario, "
					+ "h.valor_antigo, "
					+ "h.valor_novo, "
					+ "h.id_tipo_movimentacao, "
					+ "u.id_usuario, "
					+ "u.login, "
					+ "u.senha, "
					+ "u.cpf_cnpj_pessoa, "
					+ "ps.nome, "
					+ "ps.cpf_cnpj_pessoa, "
					+ "p.nome as nomeProduto,"
					+ "t.id_tipo_alteracao,"
					+ "t.descricao "
					+ "FROM historico h "
					+ "INNER JOIN usuario u ON u.id_usuario = h.id_usuario "
					+ "INNER JOIN pessoa ps ON ps.cpf_cnpj_pessoa = u.cpf_cnpj_pessoa "
					+ "INNER JOIN produto p ON p.id_produto = h.id_produto "
					+ "INNER JOIN tipo_alteracao t ON t.id_tipo_alteracao = h.id_tipo_movimentacao";
		
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				Historico historico = new Historico();
				historico.setId_historico(rs.getInt("id_historico"));
				historico.setData_hora(rs.getTimestamp("data_hora"));
				historico.setValor_antigo(rs.getString("valor_antigo"));
				historico.setValor_novo(rs.getString("valor_novo"));
				
				Usuario usuario = new Usuario();
				usuario.setId(rs.getInt("id_usuario"));
				usuario.setAdministrador(true);
				usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				usuario.setNome(rs.getString("nome"));
				historico.setUsuario(usuario);
				
				Produto produto = new Produto();
				produto.setId(rs.getInt("id_produto"));
				produto.setNome(rs.getString("nomeProduto"));
				historico.setProduto(produto);
				
				TipoAlteracao tipoAlteracao = new TipoAlteracao();
				tipoAlteracao.setId_tipo_alteracao(rs.getInt("id_tipo_alteracao"));
				tipoAlteracao.setDescricao(rs.getString("descricao"));
				historico.setTipo_alteracao(tipoAlteracao);
				
				listaHistorico.add(historico);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listaHistorico;
	}
	
	@Override
	public TipoAlteracao getTipoAlteracao(Integer idTipoAlteracao) {
		
		String sql = "SELECT descricao, id_tipo_alteracao FROM tipo_alteracao WHERE id_tipo_alteracao = ?";
		
		try {
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, idTipoAlteracao);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				TipoAlteracao tipoAlteracao = new TipoAlteracao();
				tipoAlteracao.setDescricao(rs.getString("descricao"));
				tipoAlteracao.setId_tipo_alteracao(rs.getInt("id_tipo_alteracao"));
				return tipoAlteracao;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Historico> filtrar(String nomeProduto, String datainicial, String dataFinal) {
		List<Historico> listaHistoricos = new ArrayList<Historico>();
		
		String sql = "SELECT "
				+ "h.id_historico, "
				+ "h.data_hora, "
				+ "h.id_produto, "
				+ "h.id_usuario, "
				+ "h.valor_antigo, "
				+ "h.valor_novo, "
				+ "h.id_tipo_movimentacao, "
				+ "u.id_usuario, "
				+ "u.login, "
				+ "u.senha, "
				+ "u.cpf_cnpj_pessoa, "
				+ "pu.nome as nomeUsuario, "
				+ "pu.cpf_cnpj_pessoa, "
				+ "p.nome as nomeProduto,"
				+ "t.id_tipo_alteracao,"
				+ "t.descricao "
				+ "FROM historico h "
				+ "INNER JOIN usuario u ON u.id_usuario = h.id_usuario "
				+ "INNER JOIN pessoa pu ON pu.cpf_cnpj_pessoa = u.cpf_cnpj_pessoa "
				+ "INNER JOIN produto p ON p.id_produto = h.id_produto "
				+ "INNER JOIN tipo_alteracao t ON t.id_tipo_alteracao = h.id_tipo_movimentacao  WHERE 1=1 ";
		
		int aux 		= 0;
		int iProduto 	= 0;
		int iDatas 		= 0;
		
		if(nomeProduto != null && !nomeProduto.trim().equals("")){
			sql += " AND p.nome LIKE ? ";
			
			aux ++;
			iProduto = aux;
		}
		
		if((datainicial != null && !datainicial.trim().equals("")) && (dataFinal != null && !dataFinal.trim().equals(""))){
			sql += " AND h.data_hora BETWEEN ? AND ? ";
			
			aux ++;
			iDatas = aux;
		}
		
		try {
			PreparedStatement ps =  conn.prepareStatement(sql);
			
			if(nomeProduto != null && !nomeProduto.trim().equals("")){
				ps.setString(iProduto, "%"+nomeProduto+"%");
			}
			
			if((datainicial != null && !datainicial.trim().equals("")) && (dataFinal != null && !dataFinal.trim().equals(""))){
				ps.setTimestamp(iDatas, Timestamp.valueOf(UtilProject.formatarDataTimestamp(datainicial)));
				ps.setTimestamp(iDatas+1, Timestamp.valueOf(UtilProject.formatarDataTimestamp(dataFinal)));
			}
			System.out.println(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				
				Historico historico = new Historico();
				historico.setId_historico(rs.getInt("id_historico"));
				historico.setData_hora(rs.getDate("data_hora"));
				historico.setValor_antigo(rs.getString("valor_antigo"));
				historico.setValor_novo(rs.getString("valor_novo"));
				
				Usuario usuario = new Usuario();
				usuario.setId(rs.getInt("id_usuario"));
				usuario.setAdministrador(true);
				usuario.setId_pessoa_cpf_cnpj(rs.getString("cpf_cnpj_pessoa"));
				usuario.setNome(rs.getString("nomeUsuario"));
				historico.setUsuario(usuario);
				
				Produto produto = new Produto();
				produto.setId(rs.getInt("id_produto"));
				produto.setNome(rs.getString("nomeProduto"));
				historico.setProduto(produto);
				
				TipoAlteracao tipoAlteracao = new TipoAlteracao();
				tipoAlteracao.setId_tipo_alteracao(rs.getInt("id_tipo_alteracao"));
				tipoAlteracao.setDescricao(rs.getString("descricao"));
				historico.setTipo_alteracao(tipoAlteracao);
				
				listaHistoricos.add(historico);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		return listaHistoricos;
	}
	
}
