(function () {

    var profitData = [
        ['data1', 30, 200, 100, 400, 150, 250],
        ['data2', 50, 20, 10, 40, 15, 25]
    ];

    var requestReportData = function (reportType) {

        $.ajax("/RelatorioSV?tipoRelatorio=" + reportType, {
            method: "GET",
        }).always(function (successResponse) {
            console.log(successResponse.test);
            return successResponse;
        });
    };


    var generateChart = function(bindTo, data){
        return c3.generate({
            bindto: bindTo,
            data: {
                columns: data
            }
        })
    };

    var profitChart = generateChart('#profit-chart', profitData);
    var incomingProductChart = generateChart('#incoming-product-chart', profitData);
    var outcomingProductChart = generateChart('#outgoing-product-chart', profitData);

    console.log(requestReportData("profit"));

}).call(this);