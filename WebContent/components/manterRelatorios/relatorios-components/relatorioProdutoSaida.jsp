<%--
  Created by IntelliJ IDEA.
  User: leodsgn
  Date: 9/20/16
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8" %>

<section class="col l6">
    <div class="card-panel chart-panel">
        <header class="row">
            <div class="col s6">
                <h5 class="left">Saída de Produtos</h5>
            </div>
            <div class="col s6">
                <button type="button" class="waves-effect waves-light btn red right chart-button">Gerar PDF</button>
            </div>
        </header>
        <div id="outgoing-product-chart" class="canvas row">
        </div>
    </div>
</section>
