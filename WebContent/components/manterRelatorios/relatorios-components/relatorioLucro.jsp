<%--
  Created by IntelliJ IDEA.
  User: leodsgn
  Date: 9/20/16
  Time: 16:38
  To change this template use File | Settings | File Templates.
--%>

<section id="profit-panel" class="col s12">
    <div class="card-panel chart-panel">
        <header class="row">
            <div class="col s6">
                <h5 class="left">Relatório de Lucro</h5>
            </div>
            <div class="col s6">

                <div class="col s6">
                    <div>
                        <h5>R$ 23.239.123,00</h5>
                        <span class="text right-align chart-date">16 Set 2016</span>
                    </div>
                </div>
                <div class="col s6">
                    <button type="button" class="waves-effect waves-light btn red chart-button">Gerar PDF</button>
                </div>

            </div>
        </header>
        <div id="profit-chart" class="canvas row">
        </div>
        <footer class="footer-chart" class="row">
            <ul class="tabs blue-grey lighten-5" width: 100%;>
                <li tag="home" class="tab col s2 blue-grey-text text-darken-3"><a href="#">Dia</a></li>
                <li tag="usuario" class="tab col s2 blue-grey-text text-darken-3"><a href="#">Semana</a></li>
                <li tag="produto" class="tab col s2 blue-grey-text text-darken-3"><a href="#">Mês</a></li>
                <li tag="fornecedor" class="tab col s2 blue-grey-text text-darken-3"><a href="#">Ano</a></li>
            </ul>
        </footer>
    </div>
</section>