<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
<meta charset="UTF-8">
<title>Histórico do produto</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</head>

<body>
	<div class="row">
		<div class="col s6">
			<h4>Histórico dos Produtos</h4>	
		</div>
	</div>
    		
   	<div class="row">
   		<form action="HistoricoSV" method="post" role="form">
   			<input type="hidden" name="acao" value="filtrar">
	   		<div class="col s12">
	   			<div class="row">
					<div class="input-field col s5">
				        <input name="nomeProduto" id="last_name" type="text" class="validate">
				        <label for="last_name">Nome Produto</label>
			      	</div>
			      	<div class="input-field col s2">
			      		<input name="dataInicial" type="date">
			      	</div>
			      	
			      	<div class="input-field col s2">
			      		  <input name="dataFinal" type="date">
			      	</div>
		      	</div>	
			</div>
			<div class="col s12" style="text-align: right;">
	 			<button type="submit" class="waves-effect waves-light btn  grey darken-1">filtrar</button>
	  		</div>
  		</form>
   	</div>
   	      	
   	<div class="row">
   	      
   		<div class="col s12">
   	      	<table class="striped">
	        	<thead>
		        	<tr>
		            	<th style="width: 30%;" data-field="id">Produto</th>
		            	<th style="width: 20%;" data-field="name">Alteração</th>
		            	<th style="width: 10%;" data-field="name">Valor antigo</th>
		            	<th style="width: 10%;" data-field="name">Valor novo</th>
						<th style="width: 15%;" data-field="name">Data hora/Alteração</th>
						<th style="width: 25%;" data-field="name">Usuário que alterou</th>
		          	</tr>
		        </thead>
		
		        <tbody>
		        	<c:forEach items="${listaHistorico}" var="item">
			        	<tr>
							<td style="width: 30%;">${item.produto.nome}</td>
			            	<td style="width: 20%;">${item.tipo_alteracao.descricao}</td>
			            	<td style="width: 10%;">${item.valor_antigo}</td>
			            	<td style="width: 10%;">${item.valor_novo}</td>
							<td style="width: 10%;">${item.getData_hora_formatada()}</td>
							<td style="width: 25%;">${item.usuario.nome}</td>
			          	</tr>
		          	</c:forEach>
		          	<tr><td colspan="6">${msgHistorico}</td></tr><!-- Caso a listaHistorico venha vazia, mostrar a mensagem de 'nenhum historico encontrado' -->
		        </tbody>
	    	</table>
   		</div>
	</div>
</body>

</html>