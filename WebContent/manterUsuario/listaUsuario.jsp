<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="row">
		<div class="col s6">
			<h4>Listagem de usuários</h4>	
		</div>
	</div>
    		
   	<div class="row">
   		<form action="UsuarioSV" method="post" role="form">
   			<input type="hidden" name="acao" value="filtrar">
	   		<div class="col s6">
				<div class="input-field col s12">
			        <input name="nomeFiltrar" id="last_name" type="text" class="validate">
			        <label for="last_name">Nome</label>
		      	</div>
			</div>
			<div class="col s12" style="text-align: right;">
	 			<button type="submit" class="waves-effect waves-light btn  grey darken-1">filtrar</button>
	 			<a class="waves-effect waves-light btn modal-trigger" href="UsuarioSV?acao=novo" >Criar</a>
	  		</div>
  		</form>
   	</div>
   	      	
   	<div class="row">
   	      
   		<div class="col s12">
   	      	<table class="striped">
	        	<thead>
		        	<tr>
		            	<th data-field="id">Nome</th>
		            	<th data-field="name">login</th>
		            	<th data-field="name">email</th>
		            	<th data-field="name">telefone</th>
		          	</tr>
		        </thead>
		
		        <tbody>
		        	<c:forEach items="${listaUsuario}" var="item">
			        	<tr>
							<td style="width: 30%;">${item.nome}</td>
			            	<td style="width: 20%;">${item.login}</td>
			            	<td style="width: 20%;">${item.email}</td>
			            	<td style="width: 10%;">${item.telefone_ddd}${item.telefone_numero}</td>
			            	<td style="width: 30%;text-align: right;">
				            	<a href="UsuarioSV?acao=editar&id=${item.id}" class="waves-effect waves-light btn "><i class="material-icons ">mode_edit</i></a>
				            	<a href="UsuarioSV?acao=excluir&id=${item.id}" onclick="confirm('Deseja realmente excluir este item?');" class="waves-effect waves-light btn red darken-4"><i class="material-icons">delete</i></a>
			            	</td>
			          	</tr>
		          	</c:forEach>
		          	<tr><td colspan="5">${msgUsuario}</td></tr><!-- Caso a listaUsuario venha vazia, mostrar a mensagem de 'nenhum usuário encontrado' -->
		        </tbody>
	    	</table>
   		</div>
	</div>
	
	<!-- MODAL QUE APARECE QUANDO FOR CADASTRAR O USUÁRIO -->
	<input id="abrirModalUsuario" type="hidden" class=" modal-trigger" href="#modalUsuario" />
	<div id="modalUsuario" class="modal" style="max-height: 100%; width:80%;">
		<c:import url="manterUsuario/Usuario.jsp"></c:import>
	</div>
	
</body>
</html>