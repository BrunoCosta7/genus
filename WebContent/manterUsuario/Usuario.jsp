<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<form action="UsuarioSV" method="post" role="form">
	<input type="hidden" name="acao" value="salvar">
    <div class="modal-content">
    	<span>Cadastrando novo usuário</span>
    	<div>
	    	<div id="tabsUsuario" class="col s12" style="height: 90px">
		    	<ul class="tabs grey lighten-5">
			      	<li class="tab col s2"><a class="active" href="#tabGeralUsuario">Geral</a></li>
			      	<li class="tab col s2"><a href="#tabEnderecoUsuario">Endereço</a></li>
		      	</ul>
		    </div>
		    <input value="${usuario.id}"  type="hidden" class="validate" name="id">
	    	<div id="tabGeralUsuario" class="col s12">
	    		<div class="row">
			    	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.nome}" id="inputName" type="text" class="validate" name="nome">
						        <label for="inputName">Nome</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.id_pessoa_cpf_cnpj}" id="inputCodigo" type="text" class="validate" name="cpfCnpj">
						        <label for="inputCodigo">CPF/CNPJ</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
	   	      		<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.telefone_ddd}${usuario.telefone_numero}" id="inputTelefone" type="text" class="validate" name="telefone">
						        <label for="inputTelefone">Telefone</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.email}" id="inputEmail" type="text" class="validate" name="email">
						        <label for="inputEmail">Email</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
	   	      		<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.login}" id="inputLogin" type="text" class="validate" name="login">
						        <label for="inputLogin">Login</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.senha}" id="inputSenha" type="password" class="validate" name="senha">
						        <label for="inputSenha">Senha</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
   	      		<div class="row">
   	      			<div class="col s6">
		   	      		<div class="col s12">
		   	      			<label>ADMINISTRADOR</label>
				   	      	<div class="switch">
						    	<label>Não<input ${administrador} type="checkbox" name="administrador"><span class="lever"></span>Sim</label>
						  	</div>
					  	</div>
				  	</div>
				  	<div class="col s6">
		   	      		<div class="col s12">
		   	      			<label>ATIVO</label>
				   	      	<div class="switch">
						    	<label>Não<input ${ativo} type="checkbox" name="ativo"><span class="lever"></span>Sim</label>
						  	</div>
					  	</div>
				  	</div>
	   	      	</div>
	    	</div>
	    	
	    	<div id="tabEnderecoUsuario" class="col s12">
	    		<div class="row">
			    	<div class="col s12">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.endereco.logradouro}" id="inputLogradouro" type="text" class="validate" name="enderecoLogradouro">
						        <label for="inputLogradouro">Logradouro</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
			    	<div class="col s12">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.endereco.complemento}" id="inputComplemento" type="text" class="validate" name="enderecoComplemento">
						        <label for="inputComplemento">Complemento</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
			    	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.endereco.numero}" id="inputNumero" type="text" class="validate" name="enderecoNumero">
						        <label for="inputNumero">Número</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${usuario.endereco.bairro}" id="inputBairro" type="text" class="validate" name="enderecoBairro">
						        <label for="inputBairro">Bairro</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	
	   	      	<div class="row">
	   	      	
		   	      	<div class="col s6">
			   	      		<div class="col s12">
								<div class="input-field col s12">
							        <input value="${usuario.endereco.cidade.estado.nome_estado}" id="inputEstado" type="text" class="validate" name="enderecoEstado">
							        <label for="inputEstado">Estado</label>
						      	</div>
							</div>
			   	      	</div>
			   	      	
			   	      	<div class="col s6">
			   	      		<div class="col s12">
								<div class="input-field col s12">
							        <input value="${usuario.endereco.cidade.nome_cidade}" id="inputCidade" type="text" class="validate" name="enderecoCidade">
							        <label for="inputCidade">Cidade</label>
						      	</div>
							</div>
			   	      	</div>
			   	      	
			   	      	
<!-- 		   	      	<div class="col s6"> -->
<!-- 		   	      		<div class="col s12"> -->
<!-- 					      	 <div class="input-field col s12"> -->
<!-- 							    <select name="selectEstados"> -->
<%-- 							    	<c:forEach items="${listaEstados}" var="item"> --%>
<%-- 									      <option value="${item.id_estado}"></option> --%>
<%-- 							      	</c:forEach> --%>
<!-- 						    	</select> -->
<!-- 							    <label>Estados</label> -->
<!-- 							  </div> -->
<!-- 						</div> -->
<!-- 		   	      	</div> -->
<!-- 		   	      	<div class="col s6"> -->
<!-- 		   	      		<div class="col s12"> -->
<!-- 					      	 <div class="input-field col s12"> -->
<!-- 							    <select name="selectCidades"> -->
<%-- 							    	<c:forEach items="${listaCidades}" var="item"> --%>
<%-- 									      <option value="${item.nome_cidade}"></option> --%>
<%-- 							      	</c:forEach> --%>
<!-- 						    	</select> -->
<!-- 							    <label>Cidades</label> -->
<!-- 							  </div> -->
<!-- 						</div> -->
<!-- 		   	      	</div> -->
	   	      	</div>
	    	</div> 
      	</div>
    </div>
    <div class="modal-footer">
   	  	<a href="UsuarioSV?acao=voltar" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
   	  	<button type="submit" class="waves-effect waves-light btn" href="#modalUsuario" >Salvar</button>
    </div>
   </form>
</body>
</html>