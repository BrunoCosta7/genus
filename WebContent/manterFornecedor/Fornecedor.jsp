<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <script>
	    function somenteNumeros(num) {
	        var er = /[^0-9.]/;
	        er.lastIndex = 0;
	        var campo = num;
	        if (er.test(campo.value)) {
	          campo.value = "";
	        }
	    }
	 </script>
 </head>
<body>
<form action="FornecedorSV" method="post" role="form">
	<input type="hidden" name="acao" value="salvar">
    <div class="modal-content">
    	<span>${tituloPagina}</span>
    	<div>
	    	<div id="tabsFornecedor" class="col s12" style="height: 90px">
		    	<ul class="tabs grey lighten-5">
			      	<li class="tab col s2"><a class="active" href="#tabGeralFornecedor">Geral</a></li>
			      	<li class="tab col s2"><a href="#tabEnderecoFornecedor">Endereço</a></li>
		      	</ul>
		    </div>
		    <input value="${fornecedor.id}"  type="hidden" class="validate" name="id">
	    	<div id="tabGeralFornecedor" class="col s12">
	    		<div class="row">
			    	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.insc_estadual}" id="inputInscricaoEstadual" type="text" class="validate" name="inscricaoEstadual">
						        <label for="inputInscricaoEstadual">Inscrição Estadual</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.telefone_ddd}${fornecedor.telefone_numero}" onkeyup="somenteNumeros(this);"  id="inputTelefone" type="text" class="validate" name="telefone">
						        <label for="inputTelefone">Telefone</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	
	   	      	</div>
	   	      	<div class="row">
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.nome_fantasia}" id="inputNomeFantasia" type="text" class="validate" name="nome_fantasia">
						        <label for="inputSenha">Nome Fantasia(Empresa)</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.email}" id="inputEmail" type="text" class="validate" name="email">
						        <label for="inputEmail">Email</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
	   	      	
	   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.id_pessoa_cpf_cnpj}" id="cnpj" onkeyup="somenteNumeros(this);" type="text" class="validate" name="cpfCnpj">
						        <label for="inputCodigo">CPF/CNPJ</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.nome}" id="inputResponsavel" type="text" class="validate" name="responsavel">
						        <label for="inputReponsavel">Responsável</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
   	      		<div class="row">
				  	<div class="col s6">
		   	      		<div class="col s12">
		   	      			<label>ATIVO</label>
				   	      	<div class="switch">
						    	<label>Não<input ${ativo} type="checkbox" id ="ativo" value="sim" name="ativo"><span class="lever"></span>Sim</label>
						  	</div>
					  	</div>
				  	</div>
	   	      	</div>
	    	</div>
	    	
	    	<div id="tabEnderecoFornecedor" class="col s12">
	    		<div class="row">
			    	<div class="col s12">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.logradouro}" id="inputLogradouro" type="text" class="validate" name="enderecoLogradouro">
						        <label for="inputLogradouro">Logradouro</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
			    	<div class="col s12">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.complemento}" id="inputComplemento" type="text" class="validate" name="enderecoComplemento">
						        <label for="inputComplemento">Complemento</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
			    	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.numero}" id="inputNumero" type="text" class="validate" name="enderecoNumero">
						        <label for="inputNumero">Número</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.bairro}" id="inputBairro" type="text" class="validate" name="enderecoBairro">
						        <label for="inputBairro">Bairro</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	
	   	      	<div class="row">
	   	      	
	   	      		<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.cidade.estado.nome_estado}" id="inputEstado" type="text" class="validate" name="enderecoEstado">
						        <label for="inputEstado">Estado</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${fornecedor.endereco.cidade.nome_cidade}" id="inputCidade" type="text" class="validate" name="enderecoCidade">
						        <label for="inputCidade">Cidade</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	
<!-- 		   	      	<div class="col s6"> -->
<!-- 		   	      		<div class="col s12"> -->
<!-- 					      	 <div class="input-field col s12"> -->
<!-- 							    <select name="selectEstados"> -->
<%-- 							    	<c:forEach items="${listaEstados}" var="item"> --%>
<%-- 									      <option value="${item.id_estado}"></option> --%>
<%-- 							      	</c:forEach> --%>
<!-- 						    	</select> -->
<!-- 							    <label>Estados</label> -->
<!-- 							  </div> -->
<!-- 						</div> -->
<!-- 		   	      	</div> -->
<!-- 		   	      	<div class="col s6"> -->
<!-- 		   	      		<div class="col s12"> -->
<!-- 					      	 <div class="input-field col s12"> -->
<!-- 							    <select name="selectCidades"> -->
<%-- 							    	<c:forEach items="${listaCidades}" var="item"> --%>
<%-- 									      <option value="${item.nome_cidade}"></option> --%>
<%-- 							      	</c:forEach> --%>
<!-- 						    	</select> -->
<!-- 							    <label>Cidades</label> -->
<!-- 							  </div> -->
<!-- 						</div> -->
<!-- 		   	      	</div> -->
	   	      	</div>
	    	</div> 
      	</div>
    </div>
    <div class="modal-footer">
   	  	<a href="FornecedorSV?acao=voltar" class="modal-action modal-close waves-effect waves-green btn-flat">Cancelar</a>
   	  	<button type="submit" class="waves-effect waves-light btn" href="#modalFornecedor" >Salvar</button>
    </div>
   </form>
</body>
</html>