<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="row">
		<div class="col s6">
			<h4>Listagem de fornecedores</h4>	
		</div>
	</div>
    		
   	<div class="row">
   		<form action="FornecedorSV" method="post" role="form">
   			<input type="hidden" name="acao" value="filtrar">
	   		<div class="col s6">
				<div class="input-field col s12">
			        <input name="nomeFiltrar" id="last_name" type="text" class="validate">
			        <label for="last_name">Empresa       
			        		<i class="material-icons">search</i> 
			         </label> 
			        
		      	</div>
			</div>
			<div class="col s12" style="text-align: right;">
	 			<button type="submit" class="waves-effect waves-light btn  grey darken-1">filtrar</button>
	 			<a class="waves-effect waves-light btn modal-trigger" href="FornecedorSV?acao=novo" >Inserir</a>
	  		</div>
  		</form>
   	</div>
   	      	
   	<div class="row">
   	      
   		<div class="col s12">
   	      	<table class="striped">
	        	<thead>
		        	<tr>
		            	<th data-field="id">Empresa</th>
		            	<th data-field="email">email</th>
		            	<th data-field="telefone">telefone</th>
		            	<th data-field="ativo">ativo</th>
		          	</tr>
		        </thead>
		
		        <tbody>
		        	<c:forEach items="${listaFornecedor}" var="item">
			        	<tr class="alert alert-danger">
							<td style="width: 30%;">${item.nome_fantasia}</td>
			            	<td style="width: 20%;">${item.email}</td>
			            	<td style="width: 10%;">${item.telefone_ddd}${item.telefone_numero}</td>
			            	<td style="width: 20%;">${item.ativoNome}</td>
			            	<td style="width: 30%;text-align: right;">
				            	<a href="FornecedorSV?acao=editar&id=${item.id}" class="waves-effect waves-light btn "><i class="material-icons ">mode_edit</i></a>
				            	<a href="FornecedorSV?acao=excluir&id=${item.id}" onclick="confirm('Deseja realmente desativar este item?');" class="waves-effect waves-light btn red darken-4"><i class="material-icons">delete</i></a>
			            	</td>
			          	</tr>
		          	</c:forEach>
		          	<tr><td colspan="5">${msgFornecedor}</td></tr><!-- Caso a listaFornecedor venha vazia, mostrar a mensagem de 'nenhum fornecedor encontrado' -->
		        </tbody>
	    	</table>
   		</div>
	</div>
	
	<!-- MODAL QUE APARECE QUANDO FOR CADASTRAR O FORNECEDOR -->
	<input id="abrirModalFornecedor" type="hidden" class=" modal-trigger" href="#modalFornecedor" />
	<div id="modalFornecedor" class="modal" style="max-height: 100%; width:80%;">
		<c:import url="manterFornecedor/Fornecedor.jsp"></c:import>
	</div>
	
</body>
</html>