<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</head>
<body class="teal lighten-3">
	<form Action=EsqueceuSV METHOD=POST style="height: 100% !important;">
		<div class="container">
			<div class="row">
				<div class="col s6 offset-s3"><br><br><br><br><br><br>
					<div class="card-panel">
						<div><h4 class="center-align">Esqueceu a Senha?</h4></div>
						
						<div class="row">
       						 <div class="${msgFClass}">
          						<div class="${msgSClass}">
           							 <div class="card-content white-text">
              							<p>${msgLogin}</p>
           							 </div>
          						</div>
        					</div>
     					 </div>
     					 
						<div class="center-align">
						Ola! N�o fique envergonhado, esquecer a senha � mais normal do que voc� pensa. Para trocar sua senha insira o usu�rio e cpf sem pontos ou caracteres
						</div>
						
					    <div class="input-field">
							<i class="material-icons prefix">account_circle</i>
							<input name="username" id="icon_prefix" type="text" class="validate">
							<label for="icon_prefix">Username</label>
						</div>
						
						<div class="input-field">
							<i class="material-icons prefix">picture_in_picture</i>
							<input name="password" id="password" type="password" class="validate">
							<label for="password">CPF</label>
						</div>
						
						<div class="row">
							<div class="col 6 offset-s4">
								<button class="btn waves-effect waves-light" type="submit" name="action">Ok
								<i class="material-icons right">send</i>
								</button>
							</div>
						</div>
						
						<div class="center-align">
							<a class="center-align" href=LoginSV>Voltar</a>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>