<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>

    <title>Genus</title>

    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="lib/materialize/dist/css/materialize.min.css"/>
    <link rel="stylesheet" href="lib/c3/c3.min.css">
    <link rel="stylesheet" href="css/chart.css">

</head>
<body>
<div>
    <nav class="indigo darken-3">
        <div class="nav-wrapper">
            <div>
                <a style="padding-left: 20px;" href="#!" class="brand-logo">
                    <!-- <img style="padding-top: 7px !important; max-width: 32% !important;" src="imgenus/yuna.jpg" alt="" class="circle responsive-img"> -->
                </a>
            </div>

            <ul class="right hide-on-med-and-down">
                <li><a><i class="material-icons">search</i></a></li>
                <li><a><i class="material-icons">view_module</i></a></li>
                <li><a><i class="material-icons">refresh</i></a></li>
                <li><a><i class="material-icons">more_vert</i></a></li>
            </ul>
        </div>
    </nav>
</div>
<div>
    <div class="col s12">
        <ul id="myTabs" class="tabs  indigo darken-4" style="overflow-x: hidden">
            <li tag="home" class="tab col s2"><a style="color: #F5F5F5;" class="active" href="#home">Home</a></li>
            <li tag="usuario" class="tab col s2"><a id="" style="color: #F5F5F5;" href="#usuario">Usuários</a></li>
            <li tag="produto" class="tab col s2"><a style="color: #F5F5F5;" href="#produto">Produto</a></li>
            <li tag="fornecedor" class="tab col s2"><a style="color: #F5F5F5;" href="#fornecedor">Fornecedor</a></li>
            <li tag="relatorio" class="tab col s2"><a style="color: #F5F5F5;" href="#relatorio">Relátorios</a></li>
            <li tag="historicoProduto" class="tab col s1"><a style="color: #F5F5F5;" href="#log">Histórico dos
                produtos</a></li>
        </ul>
    </div>
    <div id="home" class="col s12">
        <div class="slider">
            <ul class="slides">
                <li>
                    <img src="imgenus/1.jpg">
                    <div class="caption center-align">
                        <h3><br><br><br>Este é o Sistema Genus</h3>
                        <h5 class="light grey-text text-lighten-3">Construído para facilitar a sua rotina de
                            gerenciamento financeiro</h5>
                    </div>
                </li>
                <li>
                    <img src="imgenus/2.jpg">
                    <div class="caption left-align">
                        <h3>Finanças Empresariais sob seu controle</h3>
                        <h5 class="light grey-text text-lighten-3">Rápido e fácil</h5>
                    </div>
                </li>
                <li>
                    <img src="imgenus/3.jpg">
                    <div class="caption right-align">
                        <h3>Faça seu controle de qualquer lugar</h3>
                        <h5 class="light grey-text text-lighten-3">com uma ótima experiência</h5>
                    </div>
                </li>
                <li>
                    <img src="imgenus/4.jpg">
                    <div class="caption center-align">
                        <h3>Disponibilizamos uma série de ferramentas que vão lhe ajudar a controlar as suas
                            finanças.</h3>
                    </div>
                </li>
            </ul>
        </div>
        <br>
    </div>

    <!-- USUÁRIO -->
    <div id="usuario" class="col s12">
        <c:import url="manterUsuario/listaUsuario.jsp"></c:import>
    </div>

    <!-- PRODUTO -->
    <div id="produto" class="col s12">
        <c:import url="manterProduto/listaProduto.jsp"></c:import>
    </div>

    <!-- FORNECEDOR -->
    <div id="fornecedor" class="col s12">
        <c:import url="manterFornecedor/listaFornecedor.jsp"></c:import>
    </div>

    <!-- RELÁTORIOS -->
    <div id="relatorio" class="col s12">
        <c:import url="components/manterRelatorios/relatorios.jsp"></c:import>
    </div>

    <!-- LOG PRODUTO -->
    <div id="log" class="col s12">
        <c:import url="manterHistorico/listaHistorico.jsp"></c:import>
    </div>
    <input id="abaSelecionada" value="${abaSelecionada}" type="hidden"/>
</div>
<script type="text/javascript" src="lib/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="lib/materialize/dist/js/materialize.min.js"></script>
<script type="text/javascript" src="lib/d3/d3.min.js"></script>
<script type="text/javascript" src="lib/c3/c3.min.js"></script>

<script type="text/javascript" src="src/service.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $(".dropdown-button").dropdown();
        $('.modal-trigger').leanModal();
        $(".button-collapse").sideNav();
        $('ul.tabs').tabs().tabs('select_tab', 'tab_id');
        $('.slider').slider({full_width: true})
        $('.slider').slider('pause');
        $('.slider').slider('start');
        $('.slider').slider('next');
        $('.slider').slider('prev');

        var aba = $("#abaSelecionada").val();

        if (aba != '') {
            $('#myTabs li').each(function () {

                var tab = $(this).attr('tag');

                if (tab == aba) {
                    $(this).find('a').click();
                } else {
                    $(this).find('a').removeClass("active");
                }

            });
        }

        var ok = ${abrirModal} "";
        if (ok != null && ok == true) {
            console.log("abrirModal");
            $('#abrirModal' + '${modalcu}').click();
            ok = false;
        }


        var msg = '${msg}';
        console.log('Mensagem:' + msg);
        Materialize.toast(msg, 4000)
        msg = '';

    });

</script>
</body>
</html>