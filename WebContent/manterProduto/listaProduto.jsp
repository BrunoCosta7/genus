<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>
	<div class="row">
		<div class="col s6">
			<h4>Listagem de produtos</h4>	
		</div>
	</div>
	<form action="ProdutoSV" method="post" role="form">
		<input type="hidden" value="filtrar" name="acao"/>
	   	<div class="row">
	      	<div class="col s6">
	   			<div class="input-field col s12">
			        <input id="last_name" type="text" class="validate" name="nomeProdutoFiltrar">
			        <label for="last_name">Descrição</label>
		      	</div>
	      	</div>
	      	
	      	<div class="col s3">
	      		<div class="input-field col s12">
	        		<input id="last_name" type="text" class="validate" name="medidaProdutoFiltrar">
	        		<label for="last_name">Medida</label>
	      		</div>
	      	</div>
	      	<div class="col s3">
		     	<div class="col s12" style="text-align: right;">
					<button class="waves-effect waves-light btn  grey darken-1" type="submit">filtrar</button>
					<a class="waves-effect waves-light btn modal-trigger" href="ProdutoSV?acao=novo" >Criar</a>
				</div>
			</div>
	   	</div>
   	</form>
   	      
	<div class="row">
   	      
  		<div class="col s12">
  			<table class="striped">
		        <thead>
	          		<tr>
	              		<th>Descrição</th>
		              	<th>Fornecedor</th>
		              	<th style="text-align: center;">Qtd.Estoque</th>
		              	<th style="text-align: center;" >Preço Custo</th>
		              	<th style="text-align: center;">Preço Venda</th>
		              	<th style="text-align: center;">Ações</th>
	         		</tr>
		        </thead>
		
	        	<tbody>
	        		<c:forEach items="${listaProduto}" var="produto">
		         		<tr>
				            <td style="width: 25%;">${produto.nome}</td>
			            	<td style="width: 20%;">${produto.fornecedor.nome_fantasia}</td>
			            	<td style="width: 10%; text-align: center;">${produto.quantidade}</td>
				            <td style="width: 10%; text-align: right;">${produto.getPrecoCustoFormatado()}</td>
				            <td style="width: 10%;text-align: right;">${produto.getPrecoVendaFormatado()}</td>
				            <td style="width: 15%;text-align: right;">
				            	<a class="waves-effect waves-light btn" href="ProdutoSV?acao=editar&id=${produto.id}"><i class="material-icons ">mode_edit</i></a>
				            	<a class="waves-effect waves-light btn red darken-4" href="ProdutoSV?acao=excluir&id=${produto.id}"><i class="material-icons">delete</i></a>
			            	</td>
			          	</tr>
		          	</c:forEach>
	       		</tbody>
      		</table>
   		</div>
	</div>
	<input id="abrirModalProduto" type="hidden" class=" modal-trigger" href="#modalProduto" />
	<div id="modalProduto" class="modal" style="max-height: 100%; width:80%;">
		<c:import url="manterProduto/Produto.jsp"></c:import>
	</div>
</body>
</html>