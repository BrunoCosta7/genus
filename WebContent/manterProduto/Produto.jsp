<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<form action="ProdutoSV" method="post" role="form">
	<input type="hidden" name="acao" value="salvar">
    <div class="modal-content">
    	<span>Cadastrando novo produto</span>
    	<div>
		    <input value="${produto.id}"  type="hidden" class="validate" name="id">
	    	<div id="tabGeralUsuario" class="col s12">
	    		<div class="row">
			    	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.nome}" id="inputName" type="text" class="validate" name="nome">
						        <label for="inputName">Nome</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.medida}" id="inputCodigo" type="text" class="validate" name="medida">
						        <label for="inputCodigo">Medida</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
	   	      		<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.quantidade}" id="inputTelefone" type="text" class="validate" name="quantidade">
						        <label for="inputTelefone">Quantidade</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.fornecedor.nome_fantasia}" id="inputEmail" type="text" class="validate" name="fornecedor">
						        <label for="inputEmail">Fornecedor</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	   	      	<div class="row">
	   	      		<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.precoCusto}" id="inputLogin" type="text" class="validate" name="precoCusto">
						        <label for="inputLogin">Preço de custo</label>
					      	</div>
						</div>
		   	      	</div>
		   	      	<div class="col s6">
		   	      		<div class="col s12">
							<div class="input-field col s12">
						        <input value="${produto.precoVenda}" id="inputSenha" type="text" class="validate" name="precoVenda">
						        <label for="inputSenha">Preço de venda</label>
					      	</div>
						</div>
		   	      	</div>
	   	      	</div>
	    	</div>
      	</div>
    </div>
    <div class="modal-footer">
   	  	<a href="ProdutoSV?acao=voltar" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
   	  	<button type="submit" class="waves-effect waves-light btn" href="#modalProduto" >Salvar</button>
    </div>
   </form>
</body>
</html>