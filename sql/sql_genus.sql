﻿drop table usuario;
drop  table endereco cascade ;
drop table pessoa;
drop table cidade;
drop table estado;


create table estado(
	id_estado varchar(2) not null unique,
	nome_estado varchar(45)

);

create table cidade(
	id_cidade serial not null unique,
	nome_cidade varchar(45),
	id_estado varchar not null,
	foreign key(id_estado)
	references estado(id_estado)
	
);

create table endereco(
	id serial not null unique,
	logradouro varchar(45),
	numero int,
	id_pessoa_cpf_cnpj varchar(14),
	id_usuario int,
	complemento varchar(45) not null,
	bairro varchar(45),
	id_cidade int not null ,
	primary key(id),
	foreign key(id_cidade)
	references cidade(id_cidade)

);

create table pessoa(
	cpf_cnpj_pessoa varchar(14) not null unique,
	nome varchar(100),
	telefone_ddd varchar(2),
	telefone_numero varchar (10),
	email varchar(45),
	insc_estadual varchar(15),
	id_endereco int not null,
	primary key(cpf_cnpj_pessoa),
	foreign key(id_endereco)
	references endereco(id)
);

create table usuario(
	id_usuario serial not null,
	login varchar(30) not null,
	senha varchar(30) not null,
	administrador boolean not null,
	ativo boolean not null,
	cpf_cnpj_pessoa varchar not null,
	primary key(id_usuario),
	foreign key(cpf_cnpj_pessoa)
	references pessoa(cpf_cnpj_pessoa)

);



create table fornecedor(
    id_Fornecedor serial not null,
    nome_fantasia VARCHAR(45)not null,
    ativo BOOLEAN,
    cpf_cnpj_pessoa VARCHAR(14)not null,
    primary key(id_Fornecedor),
    foreign key(cpf_cnpj_pessoa)
    references Pessoa(cpf_cnpj_pessoa)
);




insert into "estado" ("id_estado","nome_estado") values('SP','São Paulo');
insert into "estado" values('GO','goias');
insert into "cidade" values(70,'anapolis','GO');
insert into "endereco" values(70,'rua augusta',1,70,70,'bairro','sla',70);
insert into "pessoa" values(70,'70105574112','jonathan','62',1,null,'33133096','jonathan.melosouza@gmail.com',
'1234',70);
insert into "usuario" values(70,'user','passoword',true,true,70);


delete from endereco  where id = 2 ;
delete from pessoa  where cpf_cnpj_pessoa = '2';

select * from usuario
select * from endereco
select * from pessoa
select * from cidade
select * from estado

select * from estado where id_estado = SP



